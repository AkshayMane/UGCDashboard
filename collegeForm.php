
<?php
require_once 'head.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <a class="btn btn-sm btn-primary pull-left" href="index.php">Back</a>
      <h1 style="text-align: center;">
        Fill your Dashboard details here
        <small></small>
      </h1>
      <div class="alert alert-dismissible" id="alertBox" style="display: none" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> <span id="alertMsg"></span></h4>
      </div>
          </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <form method="POST" id="colg_form">
          <div class="col-md-12 col-md-offset-0">
            <!-- general form elements -->

            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">College Details</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
                <div class="box-body">
                    <div class="form-group">
                      <label for="collName">College Name</label>
                      <input name="name" type="text" class="form-control" id="name" value="" placeholder="Enter College Name">
                    </div>

                  <div class="form-group">
                    <label for="collCode">College code</label>
                    <input name="colg_code" type="text" class="form-control" id="colg_code" value="" placeholder="Enter College code">
                  </div>
                  <div class="form-group">
                    <label for="collCode">Contact</label>
                    <input name="contact" type="text" class="form-control" id="contact" value="" placeholder="Enter College Contact">
                  </div><div class="form-group">
                    <label for="collCode">College Email ID</label>
                    <input name="mail" type="email" class="form-control" id="mail" value="" placeholder="Enter College email Id">
                  </div><div class="form-group">
                    <label for="collCode">Password</label>
                    <input name="password" type="password" class="form-control" id="password" value="" placeholder="Enter Password">
                  </div>
                  <div class="form-group">
                    <label for="collYear">Year of Establishment</label>
                    <input name="yoe" type="text" class="form-control" id="yoe" value="" placeholder="Enter Year of Establishment">
                  </div>
                    <div class="form-group">
                      <label for="collAdd">College Address</label>
                      <input name="address" type="text" class="form-control" id="address" value="" placeholder="Enter College Address">
                    </div>
                    <div class="form-group">
                      <label for="collPin">Pin Code</label>
                      <input name="pincode" type="text" class="form-control" id="pincode" value="" placeholder="Enter Pin Code">
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                      <label for="naac">NAAC status</label>
                      <select name="naac" class="form-control" style="width: 70%" id="naac">
                        <option selected value="" disabled hidden>Select Option</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                      <label for="2fstatus">2(f) status</label>
                      <select name="12f" class="form-control" style="width: 70%" id="12f">
                        <option selected value="" disabled hidden>Select Option</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                      <label for="12b">12(b) status</label>
                      <select name="12b" class="form-control" style="width: 70%" id="12b">
                        <option selected value="" disabled hidden>Select Option</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                      </select>
                    </div>

                  </div>
                <!-- /.box-body -->

            </div>


            <!-- general form elements -->
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">College Details Part 2</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
                <div class="box-body">

                  <div class="form-group col-md-4 col-xs-12">
                    <label for="collUnder">College under</label>
                    <select name="gov" class="form-control" style="width: 70%" id="gov">
                      <option selected value="" disabled hidden>Select Option</option>
                      <option value="1">Government</option>
                      <option value="0">Non-Government</option>
                    </select>
                  </div>

                  <div class="form-group col-md-4 col-xs-12">
                    <label for="collZone">Select Zone</label>
                    <select  name="zone" class="form-control" style="width: 70%" id="zone">
                      <option selected value="" disabled hidden>Select Option</option>
                      <option value="N">North</option>
                      <option value="S">South</option>``
                      <option value="E">East</option>
                      <option value="W">West</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4 col-xs-12">
                    <label for="collParent">Parent University</label>
                    <select name="uni_id" class="form-control" style="width: 70%" id="uni_id">
                      <option selected value="" disabled hidden>Select Option</option>
                    </select>
                  </div>

                  <div class="form-group col-md-4 col-xs-12">
                    <label for="natureOfAffiliation">Nature of Affiliation</label>
                    <select name="affiliation" class="form-control" style="width: 70%" id="affiliation">
                      <option selected value="" disabled hidden>Select Option</option>
                      <option value="T">Temporary</option>
                      <option value="P">Permanent</option>
                    </select>
                  </div>

                  <div class="form-group col-md-4 col-xs-12">
                    <label for="teachingUpto">Teaching Upto</label>
                    <select name="edu_level" class="form-control" style="width: 70%" id="edu_level">
                      <option selected value="" disabled hidden>Select Option</option>
                      <option value="H">High Secondary</option>
                      <option value="B">UG</option>
                      <option value="M">PG</option>
                      <option value="P">PHD</option>
                    </select>
                  </div>

                  <div class="form-group col-md-4 col-xs-12">
                    <label for="organisation">Organisation</label>
                    <select name="aided" class="form-control" style="width: 70%" id="aided">
                      <option selected value="" disabled hidden>Select Option</option>
                      <option value="A">Aided</option>
                      <option value="U">Unaided</option>
                      <option value="">None</option>
                    </select>
                  </div>

                </div>
                <!-- /.box-body -->
                <!--
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                -->
            </div>
            <!-- /.box -->
            <?php 
            	if(isset($_GET['colg_id']) && $_GET['colg_id'] != ''){
            		echo '<input name="colg_id" value="'.$_GET['colg_id'].'" style="display:none" />';
            	}
            ?>

            <div class="">
              <button type="submit" class="btn btn-lg btn-primary col-md-offset-5 col-md-2">Submit</button>
            </div>

          </div>
        </form>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php
if(!isset($_GET['colg_id'])){ ?>
<script src="js/insertCollege.js"></script>  
<?php 
}else{ ?>
<script src="js/updateCollege.js"></script>
<?php }?>

<script src="js/dropdown.js"></script>
</body>
</html>
