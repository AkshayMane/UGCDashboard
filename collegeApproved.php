<?php
	require_once 'api/connection.php';
	require_once 'head.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div id="header"></div>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      	College List  
      </h1>     <?php
     if(isset($_SESSION['colg_approved'])){
     ?>
      <div class="alert alert-dismissible alert-success " id="alertBox" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> <span id="alertMsg">College aprroved Successfully</span></h4>
      </div>
      <?php
      	unset($_SESSION['colg_approved']);
      	}
      ?>
     </section>

     <div class="content">
	     	<div class="box">
	     		<div class="box-body">
	     			<div class="dataTables_wrapper form-inline dt-bootstrap">
	     					 <table id="collegeTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		                <thead  id=collegeHead>
		                <td>Name</td>
		                <td>NAAC</td>
		                <td>Year of Establishment</td>
		                <td>State</td>
		                <td>Contact</td>
		                <td>Email</td>
		                <td>Is Approved..!</td>

		        </thead>
		        <tbody id="collegeDataRow">

                </tbody>
              </table>
	     			</div>
	     		</div> 
	     	</div>
	     </div>
	 </div>
<div id="footer"></div>
</div>

</div>

<script type="text/javascript" src="js/collegeTable.js"></script>
<script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php")

    });
</script>
</body>