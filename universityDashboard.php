
<?php

require_once 'head.php';
?>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<div id="header"></div>
  <style>
    .f1{align:center; font-family: 'Oxygen', sans-serif;}
    table{text-align: center;}

  </style>
  <!-- Left side column. contains the logo and sidebar -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">universityDashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6 ">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="name"></h3>

              <p>University Name</p>
            </div>
            <div class="icon">
              <i class="ion-university ion"></i>
            </div>
            <a href="#" class="small-box-footer"> <i></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="uni_code"></h3>

              <p>University Code</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-bookmarks"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="zone"></h3>

              <p>Zone</p>
            </div>
            <div class="icon">
              <i class="ion ion-location"></i>
            </div>
            <a href="#" class="small-box-footer"> <i></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="stateName"></h3>

              <p>State</p>
            </div>
            <div class="icon">
              <i class="ion ion-map"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>


      <div class="row">
              <div class="col-lg-3 col-xs-6 ">
                <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3 id="totalcolleges"></h3>

                    <p>Total Colleges</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-university"></i>
                  </div>
                  <a href="#" class="small-box-footer"> <i></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3 id="totalstudents"></h3>

                    <p>Total Students</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                  </div>
                  <a href="#" class="small-box-footer"> <i ></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3 id="topcolleges"></h3>

                    <p>Minority Students</p>

                  </div>
                  <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                  </div>
                  <a href="#" class="small-box-footer"> <i></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                  <div class="inner">
                    <h3 id="districtName"></h3>

                    <p>District</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-map"></i>
                  </div>
                  <a href="#" class="small-box-footer"> <i ></i></a>
                </div>
              </div>
              <!-- ./col -->
            </div>


        <div class="col-md-6 col-xs-12">
            <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">Top Colleges under University (NAAC approved)</h3>
                          <select class="form-control pull-right" style="width: 40%" id="year">
                                                              <option selected disabled hidden>Select Year</option>
                                                              <option value="2010">2010</option>
                                                              <option value="2011">2011</option>
                                                              <option value="2012">2012</option>
                                                              <option value="2013">2013</option>
                                            </select>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                          <table class="table table-striped">
                            <tr>
                              <th style="width: 50px">#</th>
                              <th style="padding-left: 140px ">College Name</th>
                              <th style="padding-left: 100px ">City</th>
                              <th style="width: 40px">Label</th>
                            </tr>
                            <tr>
                              <td>1.</td>
                              <td>College1</td>
                               <td>City1</td>
                              <td><span class="badge bg-red">55%</span></td>
                            </tr>
                            <tr>
                              <td>2.</td>
                              <td>College2</td>
                              <td>City2</td>
                              <td><span class="badge bg-yellow">70%</span></td>
                            </tr>
                            <tr>
                              <td>3.</td>
                              <td>College3</td>
                              <td>City3</td>
                              <td><span class="badge bg-light-blue">30%</span></td>
                            </tr>
                            <tr>
                              <td>4.</td>
                              <td>College4</td>
                              <td>City4</td>
                              <td><span class="badge bg-green">90%</span></td>
                            </tr>
                          </table>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
               </div>



                <div class="col-md-6 col-xs-12">
                           <div class="box">
                                         <div class="box-header with-border">
                                           <h3 class="box-title">College </h3>

                                           <div class="box-tools pull-right">
                                             <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                             </button>
                                             <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                           </div>
                                         </div>
                                         <!-- /.box-header -->
                                         <div class="box-body">
                                           <div class="row">
                                             <div class="col-md-8">
                                               <div class="chart-responsive">
                                                 <canvas id="uniCanvas" width="225" height="225"></canvas>
                                               </div>
                                               <!-- ./chart-responsive -->
                                             </div>
                                             <!-- /.col -->
                                             <div class="col-md-4">
                                               <ul class="chart-legend clearfix">
                                                 <li><i class="fa fa-circle-o text-red"></i> A</li>
                                                 <li><i class="fa fa-circle-o text-green"></i> B </li>
                                                 <li><i class="fa fa-circle-o text-blue"></i> C</li>
                                                 <li><i class="fa fa-circle-o text-yellow"></i> D</li>

                                               </ul>
                                             </div>
                                             <!-- /.col -->
                                           </div>
                                           <!-- /.row -->
                                         </div>
                                         <!-- /.box-body -->

                           </div>
                           </div>




     <div class="row">
        <div class=" col-sm-6 col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Caste wise Student Proportion</h3>
              <select class="form-control pull-right" style="width: 40%" id="year">
                                    <option selected disabled hidden>Select Year</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013">2013</option>
                  </select>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
               <tr>
                                                <th style="width: 10px">#</th>
                                                <th style="padding-left: 150px ">Category</th>
                                                <th>Ratio</th>
                                                <th style="width: 40px">Percentage</th>
                                                <th style="width: 50px">Out of</th>
                                              </tr>
                                              <tr>

                  <td>1.</td>
                  <td>SC Students</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-primary" style="width: 9%"></div>
                    </div>
                  </td>
                  <td><span >6%</span></td>
                  <td><span >28</span></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>ST Students</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-success" style="width: 10%"></div>
                    </div>
                  </td>
                  <td><span >5%</span></td>
                  <td><span >40</span></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>NT Students</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                    </div>
                  </td>
                  <td><span >50%</span></td>
                  <td><span >500</span></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>OBC Students</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-danger" style="width: 50%"></div>
                    </div>
                  </td>
                  <td><span >50%</span></td>
                  <td><span >500</span></td>
                </tr>

                                  <td>5.</td>
                                  <td>Total Students</td>
                                  <td>
                                    <div class="progress progress-xs">
                                      <div class="progress-bar progress-bar-primary" style="width: 66%"></div>
                                    </div>
                                  </td>
                                  <td><span >75%</span></td>
                                  <td><span >660</span></td>
                                </tr>
                                <tr>
                                  <td>6.</td>
                                  <td>Minority Students</td>
                                  <td>
                                    <div class="progress progress-xs">
                                      <div class="progress-bar progress-bar-danger" style="width: 5%"></div>
                                    </div>
                                  </td>
                                  <td><span >7%</span></td>
                                  <td><span >23</span></td>
                                </tr>
                                <tr>
                                  <td>7.</td>
                                  <td>Minority proportion</td>
                                  <td>
                                    <div class="progress progress-xs">
                                      <div class="progress-bar progress-bar-yellow" style="width: 7%"></div>
                                    </div>
                                  </td>
                                  <td><span >8%</span></td>
                                  <td><span >31</span></td>
                                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-sm-6 col-xs-12">
            <div class="box" >
              <div class="box-header">
                <h3 class="box-title">Staff Proportion</h3>
                <select class="form-control pull-right" style="width: 40%" id="year">
                                                    <option selected disabled hidden>Select Year</option>
                                                    <option value="2010">2010</option>
                                                    <option value="2011">2011</option>
                                                    <option value="2012">2012</option>
                                                    <option value="2013">2013</option>
                                  </select>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-condensed">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th style="padding-left: 160px ">Category</th>
                    <th>Ratio</th>
                    <th style="width: 40px">Percentage</th>
                    <th style="width: 50px">Out of</th>
                  </tr>
                  <tr>
                    <td>1.</td>
                    <td>Adhoc</td>
                    <td>
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" style="width: 42%"></div>
                      </div>
                    </td>
                    <td><span >42%</span></td>
                    <td><span >55</span></td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>Permanent</td>
                    <td>
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 28%"></div>
                      </div>
                    </td>
                    <td><span >28%</span></td>
                    <td><span >55</span></td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>Contractual</td>
                    <td>
                      <div class="progress progress-xs progress-striped active">
                        <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                      </div>
                    </td>
                    <td><span >30%</span></td>
                    <td><span >35</span></td>
                  </tr>

                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

      </div>




    </section>


          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Quick Email</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              <form action="#" method="post">
                <div class="form-group">
                  <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" placeholder="Subject">
                </div>
                <div>
                  <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              </form>
            </div>
            <div class="box-footer clearfix">
              <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <div id="footer">
          </div>
        <section class="col-lg-5 connectedSortable">
          <!-- Map box -->

</section>
<script type="text/javascript" src="js/dashboard.js"></script>
  <script type="text/javascript" src="js/data.js"></script>
    <script type="text/javascript" src="assets/adminLTE/plugins/jquery/jquery-2.2.3.min.js"></script>


<script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php")

    });
</script>

</body>
</html>
