-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2017 at 08:53 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ugc`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE IF NOT EXISTS `bills` (
  `pid` int(11) NOT NULL,
  `billname` text NOT NULL,
  `amt_rec` double NOT NULL,
  `pay_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('P','C','I') NOT NULL DEFAULT 'I',
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `colg_stats`
--

CREATE TABLE IF NOT EXISTS `colg_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `total_stud` int(11) NOT NULL,
  `min_stud` int(11) NOT NULL,
  `min_prop` float NOT NULL,
  `sc_stud` int(11) NOT NULL,
  `st_stud` int(11) NOT NULL,
  `nt_stud` int(11) NOT NULL,
  `obc_stud` int(11) NOT NULL,
  `open_stud` int(11) NOT NULL,
  `total_staff` int(11) NOT NULL,
  `adhoc_staff` int(11) NOT NULL,
  `per_staff` int(11) NOT NULL,
  `con_staff` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE IF NOT EXISTS `college` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `address` longtext NOT NULL,
  `yoe` int(11) NOT NULL,
  `affiliation` enum('T','P') NOT NULL DEFAULT 'T',
  `edu_level` enum('M','B','P','H') NOT NULL DEFAULT 'B',
  `gov` tinyint(1) NOT NULL DEFAULT '0',
  `aided` enum('A','U','') NOT NULL,
  `12f` tinyint(1) NOT NULL DEFAULT '0',
  `12b` tinyint(1) NOT NULL DEFAULT '0',
  `naac` enum('A','B','C','D') NOT NULL DEFAULT 'B',
  `contact` varchar(15) NOT NULL,
  `mail` mediumtext NOT NULL,
  `uni_id` int(11) NOT NULL,
  `colg_code` int(11) NOT NULL,
  `password` varchar(15) CHARACTER SET latin7 COLLATE latin7_general_cs NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `isapproved` int(1) NOT NULL DEFAULT '0',
  `details` text NOT NULL,
  PRIMARY KEY (`cid`),
  KEY `uni_id` (`uni_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='track university records' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`cid`, `name`, `address`, `yoe`, `affiliation`, `edu_level`, `gov`, `aided`, `12f`, `12b`, `naac`, `contact`, `mail`, `uni_id`, `colg_code`, `password`, `pincode`, `isapproved`, `details`) VALUES
(1, 'Fr .Conceicao Rodrigues College of Engineering', 'Father Angels Ashram Road, Bandra West, Mumbai, Maharashtra 400050', 1984, 'P', 'M', 0, 'U', 1, 1, 'B', '02267114000', 'great@fragnel.ac.in', 1, 13135, '3d9188577cc9bfe', '3584', 0, ''),
(2, 'Fr . Conceicao Rodrigues Institute of Technology', 'Agnel Technical Education Complex, Father Angnel Marg, Sector 9A, Vashi, Navi Mumbai, Maharashtra 400703', 1994, 'P', 'B', 0, 'U', 0, 0, 'B', '022 2766 1924', 'mane.akshay1997@gmail.com', 1, 745, 'sagkdhfe', '', 0, ''),
(3, 'asdaf', 'asdgagl,l', 1334, 'T', 'H', 1, '', 0, 1, 'A', '9658745662', '', 1, 12345684, '123465', '6513', 0, ''),
(4, 'as', '513', 0, 'T', 'H', 1, 'A', 1, 1, 'A', '46153', 'a@gmail.com', 1, 48615, 'a38a52b7452fde4', '30', 0, ''),
(5, 'KJSEC', 'asch asln ajsn ', 2046, 'P', 'H', 1, 'A', 0, 0, 'B', '987456321', '', 1, 123456, 'Akshay123', '401126', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `code` varchar(12) NOT NULL,
  PRIMARY KEY (`did`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`did`, `name`, `code`) VALUES
(1, 'Computer Science', 'CS'),
(2, 'Information Technology', 'IT'),
(5, 'Information Science', 'IS');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `color` varchar(10) NOT NULL,
  `desc` text NOT NULL,
  PRIMARY KEY (`nid`),
  UNIQUE KEY `title` (`title`,`startdate`,`enddate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`nid`, `date`, `title`, `startdate`, `enddate`, `color`, `desc`) VALUES
(6, '2017-03-28 12:27:09', 'frc', '2017-05-10 00:00:00', '2017-05-12 00:00:00', '#FF0000', ''),
(7, '2017-04-01 04:20:45', 'Smart India Hackaton', '2017-03-28 00:00:00', '2017-03-28 20:00:00', '#FF8C00', ''),
(8, '2017-03-29 07:25:00', 'Data Structure Submission', '2017-03-28 06:00:00', '2017-03-28 09:00:00', '#008000', ''),
(9, '2017-03-29 03:53:21', 'DataBase', '2017-03-07 00:00:00', '2017-03-08 00:00:00', '#008000', ''),
(10, '2017-03-29 10:20:21', 'Data Structure Submission 12', '2017-02-13 00:00:00', '2017-02-14 00:00:00', '#FF0000', ''),
(12, '2017-03-28 12:54:53', 'Preparation', '2017-03-10 00:00:00', '2017-03-11 00:00:00', '#40E0D0', ''),
(25, '2017-03-29 07:24:50', 'eruidyh', '2017-03-14 00:00:00', '2017-03-19 00:00:00', '#FF0000', ''),
(26, '2017-03-29 10:20:41', 'Smarth Hackathon', '2017-03-21 00:00:00', '2017-04-15 00:00:00', '#008000', ''),
(27, '2017-03-29 10:21:03', 'admin', '2017-03-08 00:00:00', '2017-03-09 00:00:00', '#40E0D0', ''),
(28, '2017-03-29 10:21:13', 'safjs', '2017-03-09 00:00:00', '2017-03-10 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `instalment`
--

CREATE TABLE IF NOT EXISTS `instalment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `amt` varchar(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `instalment`
--

INSERT INTO `instalment` (`id`, `pid`, `amt`, `date`) VALUES
(1, 2, '100000', '2017-04-01 07:39:42'),
(2, 1, '1000', '2017-04-01 08:56:33'),
(3, 2, '100000', '2017-04-01 08:57:13'),
(4, 1, '1000', '2017-04-01 08:57:26'),
(5, 1, '1000', '2017-04-01 08:57:48'),
(6, 1, '1000', '2017-04-01 08:57:51'),
(7, 1, '1000', '2017-04-01 08:57:52'),
(8, 2, '100000', '2017-04-01 08:58:07'),
(9, 1, '100', '2017-04-01 08:59:15'),
(10, 1, '0000111', '2017-04-01 08:59:41'),
(11, 1, '100002', '2017-04-01 09:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `pid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `cid` int(11) NOT NULL,
  `title` text NOT NULL,
  `year` int(11) NOT NULL,
  `status` enum('A','R','P') NOT NULL DEFAULT 'P',
  `deptid` int(11) NOT NULL,
  `ramt` double NOT NULL,
  `edu_level` enum('M','P','B','H') NOT NULL,
  `type` enum('H','S','R','G') NOT NULL,
  `dos` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `leader_name` text NOT NULL,
  `leader_phone` varchar(15) NOT NULL,
  `leader_mail` mediumtext NOT NULL,
  `recamt` double NOT NULL,
  PRIMARY KEY (`pid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`pid`, `name`, `cid`, `title`, `year`, `status`, `deptid`, `ramt`, `edu_level`, `type`, `dos`, `leader_name`, `leader_phone`, `leader_mail`, `recamt`) VALUES
(1, 'Tasktracker', 1, '', 2016, 'P', 1, 100000, 'B', 'H', '2017-03-28 08:48:37', 'Akshay Mane', '9619249577', 'mane.akshay1997@gmail.com', 0),
(2, 'BookXchange', 1, '', 2015, 'P', 2, 20000, 'B', 'S', '2017-03-28 08:55:02', 'Ak', '9402020', 'asj@gmail.com', 0),
(3, 'ABC', 1, '', 2015, 'P', 2, 20000, 'B', 'R', '2017-03-29 04:18:56', 'Franky', '9876541230', 'franky@gmail.com', 0),
(4, 'xyz', 1, '', 2013, 'P', 2, 200000, 'B', 'G', '2017-03-29 04:20:07', 'Sayali', '20123654987', 'sayali@gmail.com', 0),
(5, 'Tasktracker2', 1, '', 2017, 'P', 1, 100000, 'B', 'H', '2017-03-28 08:48:37', 'Akshay Mane', '9619249577', 'mane.akshay1997@gmail.com', 0),
(6, 'Tasktracker3', 1, '', 2017, 'P', 1, 100000, 'B', 'H', '2017-03-28 08:48:37', 'Akshay Mane', '9619249577', 'mane.akshay1997@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE IF NOT EXISTS `university` (
  `uni_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `zone` enum('N','E','W','S') NOT NULL,
  `uni_code` varchar(15) NOT NULL,
  `state` varchar(128) NOT NULL,
  `email` text NOT NULL,
  `password` varchar(5) CHARACTER SET latin7 COLLATE latin7_general_cs NOT NULL,
  PRIMARY KEY (`uni_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `university`
--

INSERT INTO `university` (`uni_id`, `name`, `zone`, `uni_code`, `state`, `email`, `password`) VALUES
(1, 'Mumbai University', 'W', 'MU', 'Maharashtra', '', ''),
(2, 'Manipal', 'S', 'MNP', 'Karnatka', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mail` text NOT NULL,
  `password` text NOT NULL,
  `role` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `college`
--
ALTER TABLE `college`
  ADD CONSTRAINT `college_ibfk_1` FOREIGN KEY (`uni_id`) REFERENCES `university` (`uni_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `college` (`cid`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
