<?php
require_once 'api/connection.php';
    if(!isset($_GET['colg_id'])){
      header("Location:404Page.php");
      exit();
    }
        $id = $_GET["colg_id"];
        if($_SESSION["type"] == "college")
           if($_SESSION["cid"] != $id)
               header("Location:collegeDashboard.php?colg_id=".$_SESSION["cid"]);

       if($_SESSION["type"] == "university"){
           $id = $_SESSION["uid"];
           $sql = "select cid from college where uni_id = $id";
           $result = $con->query($sql);
           $alloweduniversity = array();
           while($row=$result->fetch_row()){
               array_push($alloweduniversity,$row[0]);
           }
           $colgid=$_GET["colg_id"];
           if(!in_array($colgid,$alloweduniversity)){
               header("Location:universityDashboard.php?uid=$id");
          }
        }
require_once 'head.php';
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<div id="header"></div>
  <style>
    .f1{align:center; font-family: 'Oxygen', sans-serif;}
    table{text-align: center;}

  </style>
  <!-- Left side column. contains the logo and sidebar -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6 ">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="naac"></h3>

              <p>NAAC</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-book"></i>
            </div>
            <a href="#" class="small-box-footer"> <i></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="12f"></h3>

              <p>2(f) status</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-bookmarks"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="12b"></h3>

              <p>12(b)</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-bookmarks-outline"></i>
            </div>
            <a href="#" class="small-box-footer"> <i></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="colg_code"></h3>

              <p>College Code</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-checkmark-circle"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
            <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="total_stud"></h3>

              <p>Total Strength</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="min_stud"></h3>

              <p>Minority Strength</p>
            </div>
            <div class="icon">
              <i class="ion-person"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="min_prop"></h3>

              <p>Minority Proportion</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="pincode"></h3>

              <p>Pin Code</p>
            </div>
            <div class="icon">
              <i class="ion ion-map"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->


       <div class="row">
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3>900</h3>

                    <p>Student Male Strength</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                  </div>
                  <a href="#" class="small-box-footer"></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3>830</h3>

                    <p>Student Female Strength</p>
                  </div>
                  <div class="icon">
                    <i class="ion-person"></i>
                  </div>
                  <a href="#" class="small-box-footer"> <i ></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>189</h3>

                    <p>Staff Male Strength</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-add"></i>
                  </div>
                  <a href="#" class="small-box-footer"> <i ></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                  <div class="inner">
                    <h3>120</h3>

                    <p>Staff Female Strength</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-map"></i>
                  </div>
                  <a href="#" class="small-box-footer"> <i ></i></a>
                </div>
              </div>
              <!-- ./col -->
            </div>
            <!-- /.row -->





      <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-building"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Name</span>
              <span class="info-box-number" id="name"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"></span>
              <span class="info-box-number" id="gov"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-building-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"></span>
              <span class="info-box-number" id="aided"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-bank"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"></span>
              <span class="info-box-number" id="uname"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->








<div class="row">
        <div class="col-md-3">
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Address</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="address">
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-3">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Year of Establishment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="yoe">
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

                <div class="col-md-3">
          <div class="box box-warning box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Teaching Upto</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="edu_level">
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

          <div class="col-md-3">
          <div class="box box-danger box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Nature of affiliation</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="affiliation">
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

</div>

     <div class="row">
                    <div class=" col-sm-6 col-xs-12">
                      <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">Caste wise Student Proportion</h3>
          <select class="form-control pull-right" style="width: 40%" id="year">
                        <option selected disabled hidden>Select Year</option>
                        <option value="2010">2010</option>
                        <option value="2011">2011</option>
                        <option value="2012">2012</option>
                        <option value="2013">2013</option>
      </select>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                          <table class="table table-condensed">
                            <tr>
                              <th style="width: 10px">#</th>
                              <th style="padding-left: 60px ">Category</th>
                              <th>Ratio</th>
                              <th style="width: 40px">Percentage</th>
                              <th style="width: 50px">Out of</th>
                            </tr>
                            <tr>
                              <td>1.</td>
                              <td>Open</td>
                              <td>
                                <div class="progress progress-xs">
                                  <div class="progress-bar progress-bar-primary" style="width: 66%"></div>
                                </div>
                              </td>
                              <td><span >75%</span></td>
                              <td><span >660</span></td>
                            </tr>
                            <tr>
                              <td>2.</td>
                              <td>SC</td>
                              <td>
                                <div class="progress progress-xs">
                                  <div class="progress-bar progress-bar-danger" style="width: 5%"></div>
                                </div>
                              </td>
                              <td><span >7%</span></td>
                              <td><span >23</span></td>
                            </tr>
                            <tr>
                              <td>3.</td>
                              <td>ST</td>
                              <td>
                                <div class="progress progress-xs">
                                  <div class="progress-bar progress-bar-yellow" style="width: 7%"></div>
                                </div>
                              </td>
                              <td><span >8%</span></td>
                              <td><span >31</span></td>
                            </tr>
                            <tr>
                              <td>4.</td>
                              <td>NT</td>
                              <td>
                                <div class="progress progress-xs progress-striped active">
                                  <div class="progress-bar progress-bar-primary" style="width: 9%"></div>
                                </div>
                              </td>
                              <td><span >6%</span></td>
                              <td><span >28</span></td>
                            </tr>
                            <tr>
                              <td>5.</td>
                              <td>OBC</td>
                              <td>
                                <div class="progress progress-xs progress-striped active">
                                  <div class="progress-bar progress-bar-success" style="width: 10%"></div>
                                </div>
                              </td>
                              <td><span >5%</span></td>
                              <td><span >40</span></td>
                            </tr>

                          </table>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
                    </div>




                    <div class="col-sm-6 col-xs-12">
                        <div class="box" >
                          <div class="box-header">
                            <h3 class="box-title">Role wise Teacher Proportion</h3>
                            <select class="form-control pull-right" style="width: 40%" id="year">
                                                                                <option selected disabled hidden>Select Year</option>
                                                                                <option value="2010">2010</option>
                                                                                <option value="2011">2011</option>
                                                                                <option value="2012">2012</option>
                                                                                <option value="2013">2013</option>
                                                              </select>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body no-padding">
                            <table class="table table-condensed">
                              <tr>
                                <th style="width: 10px">#</th>
                                <th style="padding-left: 60px ">Category</th>
                                <th>Ratio</th>
                                <th style="width: 40px">Percentage</th>
                                <th style="width: 50px">Out of</th>
                              </tr>
                              <tr>
                                <td>1.</td>
                                <td>Permanent</td>
                                <td>
                                  <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-danger" style="width: 42%"></div>
                                  </div>
                                </td>
                                <td><span >42%</span></td>
                                <td><span >55</span></td>
                              </tr>
                              <tr>
                                <td>2.</td>
                                <td>Adhoc</td>
                                <td>
                                  <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-yellow" style="width: 28%"></div>
                                  </div>
                                </td>
                                <td><span >28%</span></td>
                                <td><span >55</span></td>
                              </tr>
                              <tr>
                                <td>3.</td>
                                <td>Contractual</td>
                                <td>
                                  <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                                  </div>
                                </td>
                                <td><span >30%</span></td>
                                <td><span >35</span></td>
                              </tr>

                            </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>

                  </div>



      <!-- Table 
      <div class="row">
        <div class=" col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Grant-related Stats (Since inception)</h3>

              <div class="box-tools">

            </div>
            <!-- 
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Total Grants</th>
                  <th><span class="label label-success">Hardware</span></th>
                  <th><span class="label label-warning">Software</th>
                  <th><span class="label label-primary">Greenhouse</th>
                  <th><span class="label label-danger">Research</th>
                </tr>
                <tr>
                  <td>1004</td>
                  <td>561</td>
                  <td>263</td>
                  <td>110</td>
                  <td>70</td>
                </tr>
              </table>
            </div>
           
          </div>


           <div class="box">
            <div class="box-header">
              <h3 class="box-title">Responsive Hover Table</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
   
                </div>
              </div>
            </div>
           
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Year</th>
                  <th>Total Grants</th>
                  <th><span class="label label-success">Hardware</span></th>
                  <th><span class="label label-warning">Software</th>
                  <th><span class="label label-primary">Greenhouse</th>
                  <th><span class="label label-danger">Research</th>
                  <th>Funds allocated</th>
                  <th>Received</th>
                </tr>
                <tr>
                  <td>2011</td>
                  <td>8</td>
                  <td>1</td>
                  <td>5</td>
                  <td>2</td>
                  <td>0</td>
                  <td>2,00,000</td>
                  <td>80,000</td>
                </tr>

                <tr>
                  <td>2012</td>
                  <td>10</td>
                  <td>4</td>
                  <td>3</td>
                  <td>1</td>
                  <td>2</td>
                  <td>2,00,000</td>
                  <td>80,000</td>
                </tr>

                <tr>
                  <td>2013</td>
                  <td>8</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2,00,000</td>
                  <td>90,000</td>
                </tr>

                <tr>
                  <td>2014</td>
                  <td>9</td>
                  <td>3</td>
                  <td>3</td>
                  <td>2</td>
                  <td>1</td>
                  <td>2,00,000</td>
                  <td>1,00,000</td>
                </tr>

                <tr>
                  <td>2015</td>
                  <td>12</td>
                  <td>6</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2,00,000</td>
                  <td>1,20,000</td>
                </tr>


              </table>
            </div>
        
        </div>
      </div>
       -->







    <section class="content">
      <div class="row">
        <div class="col-md-4 col-xs-12">
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Grant-related Stats (Since inception)</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover ">
                <thead>
                <tr>
                  <th><span class="label label-success">Hardware</span></th>
                  <td id="totalH"></td>
                </tr>
                </thead>

                <tr>
                  <th><span class="label label-warning">Software</th>
                  <td id="totalS"></td>
                </tr>
                <tr>
                  <th><span class="label label-primary">Greenhouse</th>
                  <td id="totalG"></td>
                </tr>
                <tr>
                  <th><span class="label label-danger">Research</th>
                  <td id="totalR"></td>
                </tr>
                <tr>
                  <th>Total Grants</th>
                  <td id="totalGrants"></td>
                </tr>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Grant-related Stats</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="budgetCanvas" width="200" height="200"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o text-red"></i> Hardware</li>
                    <li><i class="fa fa-circle-o text-green"></i> Software </li>
                    <li><i class="fa fa-circle-o text-blue"></i> Greenhouse</li>
                    <li><i class="fa fa-circle-o text-yellow"></i> Research</li>

                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>

          </div>


            <div class=" col-sm-4 col-xs-12">
                                          <div class="box">
                                            <div class="box-header">
                                              <h3 class="box-title">Gender wise Student Proportion</h3>
                                              <select class="form-control pull-right" style="width: 30%" id="year">
                                                                                                  <option selected disabled hidden>Select Year</option>
                                                                                                  <option value="2010">2010</option>
                                                                                                  <option value="2011">2011</option>
                                                                                                  <option value="2012">2012</option>
                                                                                                  <option value="2013">2013</option>
                                                                                </select>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body no-padding">
                                              <table class="table table-condensed">
                                                <tr>
                                                  <th style="width: 10px">#</th>
                                                  <th style="padding-left: 60px ">Gender</th>
                                                  <th>Ratio</th>
                                                  <th style="width: 40px">Percentage</th>
                                                  <th style="width: 50px">Out of</th>
                                                </tr>
                                                <tr>
                                                  <td>1.</td>
                                                  <td>Male</td>
                                                  <td>
                                                    <div class="progress progress-xs progress-striped active">
                                                      <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                                    </div>
                                                  </td>
                                                  <td><span >50%</span></td>
                                                  <td><span >500</span></td>
                                                </tr>
                                                <tr>
                                                  <td>2.</td>
                                                  <td>Female</td>
                                                  <td>
                                                    <div class="progress progress-xs progress-striped active">
                                                      <div class="progress-bar progress-bar-danger" style="width: 50%"></div>
                                                    </div>
                                                  </td>
                                                  <td><span >50%</span></td>
                                                  <td><span >500</span></td>
                                                </tr>
                                              </table>
                                            </div>
                                            <!-- /.box-body -->
                                          </div>
                                          <!-- /.box -->
                                        </div>

        </div>




        <div class="section">
        <div class="row">
          <div class="col-md-7 col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Grant-related Stats (Last Five Years)</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example2" class="table table-bordered table-hover">
                <thead>

                </thead>
                <tbody>
                <tr>
                  <th>Year</th>
                  <th>Total Grants</th>
                  <th><span class="label label-success">Hardware</span></th>
                  <th><span class="label label-warning">Software</th>
                  <th><span class="label label-primary">Greenhouse</th>
                  <th><span class="label label-danger">Research</th>
                  <th>Funds allocated</th>
                  <th>Received</th>
                </tr>
                <tr>
                  <td>2011</td>
                  <td>8</td>
                  <td>1</td>
                  <td>5</td>
                  <td>2</td>
                  <td>0</td>
                  <td>2,00,000</td>
                  <td>80,000</td>
                </tr>


                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
          <div class="col-md-5 col-xs-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Grant-related Stats (Last Five Years)</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="chart-responsive">
                      <canvas id="grantCanvas" width="225" height="225"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-4">
                    <ul class="chart-legend clearfix">
                      <li><i class="fa fa-circle-o text-red"></i> 2011</li>
                      <li><i class="fa fa-circle-o text-green"></i> 2012 </li>
                      <li><i class="fa fa-circle-o text-blue"></i> 2013</li>
                      <li><i class="fa fa-circle-o text-yellow"></i> 2014</li>
                      <li><i class="fa fa-circle-o text-dark-gray"></i> 2015</li>
                    </ul>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
            </div>
          </div>
        </div>


              <div class="row">
                <div class="col-md-6">
                  <div class="box box-solid">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="box-group" id="pid">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel box box-primary">
                          <div class="box-header with-border">
                            <h4 class="box-title">
                              College Principal Details
                            </h4>
                          </div>
                          <div id="principal" class="panel-collapse collapse in">
                            <div class="box-body">
                              <p><b>Name:</b>&nbsp;&nbsp;Jay Borade</p>
                              <p><b>E-Mail:</b>&nbsp;&nbsp;jayborade@gmail.com</p>
                              <p><b>Phone:</b>&nbsp;&nbsp;+91-9797879089</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="box box-solid">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel box box-primary">
                          <div class="box-header with-border">
                            <h4 class="box-title">
                              Dean R&D Details
                            </h4>
                          </div>
                          <div id="dean" class="panel-collapse collapse in">
                            <div class="box-body">
                              <p><b>Name:</b>&nbsp;&nbsp;Kapil Jain</p>
                              <p><b>E-Mail:</b>&nbsp;&nbsp;Kjain1996.com</p>
                              <p><b>Phone:</b>&nbsp;&nbsp;+91-9797879089</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>

          
          <!-- /.box -->

        <!-- /.col -->
      <!-- /.row -->
    </section>


          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Quick Email</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              <form action="#" method="post">
                <div class="form-group">
                  <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" placeholder="Subject">
                </div>
                <div>
                  <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              </form>
            </div>
            <div class="box-footer clearfix">
              <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->

        <section class="col-lg-5 connectedSortable">
          <!-- Map box -->

</section>
      <div id="footer">
      </div>

<script type="text/javascript" src="js/dashboard.js"></script>
      <script type="text/javascript" src="js/colstats.js"></script>

<script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php");
        $("#header").load("header.php");

    });
</script>
      <script type="text/javascript" src="js/data.js"></script>

</body>
</html>
