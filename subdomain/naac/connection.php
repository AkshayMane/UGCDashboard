<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 4/2/2017
 * Time: 12:45 PM
 */

const DBNAME ="naac";
const USERNAME="root";
const PASSWORD="";
const SERVERNAME="localhost";

// Create connection
$con = new mysqli(SERVERNAME, USERNAME,PASSWORD,DBNAME);

// Check connection
if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
}

if(!isset($_SESSION)){
    session_start();
}
?>