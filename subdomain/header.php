<?php
  require_once 'head.php';
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">

            <li class="active treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>

            <li>
              <a href="manageNotice.php">
                <i class="fa fa-calendar"></i> <span>Calendar</span>
                <span class="pull-right-container">
                  <small class="label pull-right bg-red">4</small>
                  <small class="label pull-right bg-blue">2</small>
                </span>
              </a>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Forms</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
                <li><a href="pages/forms/general.html"><i class="fa fa-circle-o text-red"></i> College</a></li>
                <li><a href="pages/forms/project.html"><i class="fa fa-circle-o text-red"></i> Project</a></li>
                <li><a href="pages/forms/NoticesAddForm.html"><i class="fa fa-circle-o text-red"></i> Add Notice</a></li>
                <li><a href="pages/forms/installment%20form.html"><i class="fa fa-circle-o text-red"></i> Installment Form</a></li>
              </ul>
            </li>

            <li>
              <a href="pages/notices.html">
                <i class="fa fa fa-files-o"></i> <span>Notices</span>
              </a>
            </li>
            <li>
              <a href="collegeApproved.php">
                <i class="fa fa-newspaper-o"></i> <span>Unapproved Colleges</span>
              </a>
            </li>
            <li>
              <a href="pages/newsLetter.html">
                <i class="fa fa-newspaper-o"></i> <span>NewsLetter</span>
              </a>
            </li>
            <!--
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
            -->
          </ul>
        </section>
        <!-- /.sidebar -->

    <!-- /.sidebar -->
</aside>