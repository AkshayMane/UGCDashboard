<?php
require_once 'bulkMail.php';
require_once 'head.php';
?>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div id="header"></div>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
				Send Email
            </h1>
        </section>
        <!--Main Content-->
        <section class="content">
            <div class="row">
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Send Quick Email To All</h3>
              <!-- tools box -->
              <!-- /. tools -->
            </div>
            <div class="box-body">
              <form action="bulkMail.php" method="post">
                <div class="form-group">
                  <input type="email" class="form-control" name="mailTo" placeholder="Email to: All colleges" disabled>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" placeholder="Subject">
                </div>
                <div>
                  <textarea name="ck" class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
                <button class="btn btn-lg btn-primary pull-right" name="send">Send</button>
              </form>
            </div>
</div>
</div>
</section>
</div>
<div id="footer">
</div>
</div><script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php");
    });
</script>
</body>
