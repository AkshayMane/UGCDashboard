<header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>U</b>GC</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>UGCWebApp</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
<!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" id="navbar-collapse" aria-expanded="false" style="height: 1px;">
                <ul class="nav navbar-nav pull-left">
                    <li><a href="UGCDashboard.php">UGCDashboard <span class="sr-only">(current)</span></a></li>
                    <li><a href="collegeDashboard.php?colg_id=1">CollegeDashboard</a></li>
                    <li><a href="logout.php"><b>Log out</b></a></li>
                </ul>
                </div>
            </div>
    </nav>
</header>


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <a href="UGCDashboard.php" style="display:inline"><span>Dashboard</span></a>
              </a>
            </li>

            <li>
              <a href="manageNotice.php">
                <i class="fa fa-calendar"></i> <span>Calendar</span>
                <span class="pull-right-container">
                  <small class="label pull-right bg-red">4</small>
                  <small class="label pull-right bg-blue">2</small>
                </span>
              </a>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Forms</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu ">
                <li><a href="pages/forms/general.html"><i class="fa fa-circle-o text-red"></i> College</a></li>
                <li><a href="pages/forms/project.html"><i class="fa fa-circle-o text-red"></i> Project</a></li>
                <li><a href="pages/forms/NoticesAddForm.html"><i class="fa fa-circle-o text-red"></i> Add Notice</a></li>
                <li><a href="pages/forms/installment%20form.html"><i class="fa fa-circle-o text-red"></i> Installment Form</a></li>
              </ul>
            </li>

            <li>
              <a href="pages/notices.html">
                <i class="fa fa fa-files-o"></i> <span>Notices</span>
              </a>
            </li>
            <li>
              <a href="collegeApproved.php">
                <i class="fa fa-newspaper-o"></i> <span>Unapproved Colleges</span>
              </a>
            </li>
            <li>
              <a href="pages/newsLetter.html">
                <i class="fa fa-newspaper-o"></i> <span>NewsLetter</span>
              </a>
            </li>
            <!--
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
            -->
          </ul>
        </section>
        <!-- /.sidebar -->

    <!-- /.sidebar -->
</aside>