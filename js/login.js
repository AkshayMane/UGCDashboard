$(document).ready(function(){



	$("#loginform").submit(function(event){

        event.preventDefault();
            var form = $(this);
            var data = form.serialize();
            $("#submitbtn").prop('disabled', true);
            $("#submitbtn").text("Verifying....");
            $.ajax({
            	url:'validate.php',
            	data:data,
            	type:'POST',
            	success:function(result){
                        console.log(result);
            		if(result=="2"){
                        var msg ="<p>Username or Password are incorrect </p>";
                        $("#login-alert").html(msg).css('display','flex');
            		}
            		else{
                        window.location.href = result;
            		}

            	},

            });
            var log='Login  <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> ';
            $("#submitbtn").prop('disabled', false);
            $("#submitbtn").html(log);
	});
});