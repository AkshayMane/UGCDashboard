<?php

require_once 'head.php';
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <div id="header"></div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
  				Projects View
            </h1>
        </section>
        <!--Main Content-->
        <section class="content">
            <div class="box">
	     		<div class="box-body">
	     			<div class="dataTables_wrapper form-inline dt-bootstrap">
	     					 <table id="collegeTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		                <thead  id=collegeHead>
		                <td>Project Name</td>
		                <td>Bill Name</td>
		                <td>amt_rec</td>
		                <td>pay_data</td>
		                <td>Status</td>

		        </thead>
		        <tbody id="collegeDataRow">

                </tbody>
              </table>
	     			</div>
	     		</div> 
	     	</div>
	     </div>
	 </div>
<div id="footer"></div>
</div>
</div>
<script type="text/javascript" src="js/projectView.js"></script>
<script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php")

    });
</script>
</body>