$(document).ready(function(){

	var head = ['name','year','ramt','Type'];
	$.ajax({
		async:true,
		cache:false,
		type:'POST',
		url:'api/getProject.php',
		success:function(result){
			var res = jQuery.parseJSON(result);
			for(var key in res){
				var project = res[key];
				var tr = $('<tr>');
				$("#collegeDataRow").append(tr);
				for(var col in head){
					var td = $('<td></td>').text(project[head[col]]);
					tr.append(td);
				}
				var td = $('<td>');
				var a1 = $('<a href="approvedProject.php?proj_id='+project['pid']+'">');
				var i1 = $('<i class="fa fa-check btn-sm btn-success approve" aria-hidden="true"></i>');
				var a2 = $('<a href="billView.php?project_id='+project['pid']+'">');
				var i2 = $('<i class="glyphicon glyphicon-menu-hamburger btn-sm btn-primary approve" aria-hidden="true"></i>');
				var center = $('<center>');
                a1.append(i1);
                a2.append(i2);
                center.append(a1,a2);
				td.append(center);
				tr.append(td);
			}
			$("#collegeTable").DataTable();
		}
	});

});