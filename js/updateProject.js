$(document).ready(function(){    
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var inputBox = ['name','year','ramt','leader_name','leader_phone','leader_mail','edu_level','deptid','type','cid'];
    var project_id = getUrlParameter('project_id');
        $.ajax({
            async : true,
            cache : false,
            url:'api/getProjById.php',
            data:{
                project_id : project_id
            },
            type:'POST',
            success:function(result){
            res = jQuery.parseJSON(result);
            var project = res[project_id];
            var college = res[project_id]['college'];
            var university = res[project_id]['college']['university'];
            for(var key in inputBox){
                $("#"+inputBox[key]).val(project[inputBox[key]]);
            }
            }
        });
        $( document ).ajaxError(function() {
            console.log("Error");
        });
        $( document ).ajaxSuccess(function() {
            console.log("success");
        });


        $("#colg_form").submit(function(event){
        console.log("Hello");
        event.preventDefault();
        var form = $(this);
        var data = form.serialize();
        console.log(data);
        $.ajax({
            async : true,
            cache : false,
            url:'api/updateProject.php',
            data:data,
            type:'POST',
            success:function(result){
        $("#alertBox").addClass("alert-success");
        $("#alertBox").css("display","block");
        $("#alertMsg").html(result);
            }
        });
        $( document ).ajaxError(function() {
            console.log("Error");
        });

    });

});