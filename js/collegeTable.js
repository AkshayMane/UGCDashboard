$(document).ready(function(){var main = {
                'affiliation' : 
                    {   
                        'P':'Permanent',
                        'T':'Temporary'
                    },
                'edu_level' :
                    {   
                        'H':'Higher Secondary',
                        'P':'PhD',
                        'M':'Masters',
                        'B':'Bachelors'
                    },
                'gov' : 
                    {
                        '1':'Government Insitute',
                        '0':'Non-Government Institute'
                    },
                'aided' :
                    {
                        'A' : 'Aided',
                        'U' : 'Un-aided',
                        '' : '-'
                    },
                '12f' :
                    {
                        '1' : 'Yes',
                        '0' : 'No'
                    },
                '12b' :
                    {
                        '1' : 'Yes',
                        '0' : 'No'
                    },
                'zone' :
                    {
                        'W' : 'West',
                        'N' : 'North',
                        'S' : 'South',
                        'E' : 'East',
                    }
            };
	var head = ['name','naac','yoe','state','contact','mail'];
	$.ajax({
		async:true,
		cache:false,
		url:'api/getNotApprovedColg.php',
		type:'POST',
		success:function(result){
			var res = jQuery.parseJSON(result);
			for(var key in res){
				var college = res[key];
				var tr = $('<tr>');
				$("#collegeDataRow").append(tr);
				for(var col in head){
					var td = $('<td></td>').text(college[head[col]]);
					tr.append(td);
				}
				var td = $('<td>');
				var a1 = $('<a href="approved.php?colg_id='+college['cid']+'">');
				var i1 = $('<i class="fa fa-check btn btn-success approve" aria-hidden="true"></i>');
				var center = $('<center>');
                a1.append(i1);
                center.append(a1);
				td.append(center);
				tr.append(td);
			}
			$("#collegeTable").DataTable();
		}
	});
});