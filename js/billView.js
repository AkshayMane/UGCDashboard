$(document).ready(function(){
	var val =  $("#project_id").val();
	var head = ['name','amt_rec','pay_date','status'];
	var pid = $("#project_id").val();
	$.ajax({
		async:true,
		cache:false,
		type:'POST',
		data:{
			project_id: pid
		},
		url:'api/getBill.php',
		success:function(result){
			var res = jQuery.parseJSON(result);
			console.log(res);
			for(var key in res){
				var project = res[key]['bill'];
				var tr = $('<tr>');
				$("#collegeDataRow").append(tr);
				for(var col in head){
					var td = $('<td></td>').text(project[head[col]]);
					tr.append(td);
				}
				var td = $('<td>');
				var a1 = $('<a href="approvedProject.php?proj_id='+project['pid']+'">');
				var i1 = $('<i class="fa fa-check btn btn-success approve" aria-hidden="true"></i>');
				var center = $('<center>');
                a1.append(i1);
                center.append(a1);
				td.append(center);
				tr.append(td);
			}
			$("#collegeTable").DataTable();
		}
	});

});