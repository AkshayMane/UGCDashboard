/**
 * Created by Franky Naidu on 4/1/2017.
 */

$(document).ready(function () {


    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        defaultDate: new Date(),
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        selectable: true,
        selectHelper: true,
        listDayFormat:true,
        allDayDefault:false,
        events: "./api/getEvents.php"
    });




});