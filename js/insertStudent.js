$(document).ready(function(){
	$("#student_form").submit(function(event){
		event.preventDefault();
		var form = $(this);
		var data = form.serialize();
		console.log(data);
		$.ajax({
			async : true,
			cache : false,
			url:'api/insertStudent.php',
			data:data,
			type:'POST',
			success:function(result){
				$("#alertBox").addClass("alert-success");
				$("#alertBox").css("display","block");
			}
		});
		$( document ).ajaxError(function() {
			console.log("Error");
		});
	});
});

