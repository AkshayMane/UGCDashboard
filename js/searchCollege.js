$(document).ready(function(){
     var sortSelect = function (select, attr, order) {
    if(attr === 'text'){
        if(order === 'asc'){
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
        }// end asc
        if(order === 'desc'){
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
        }// end desc
    }
    };
        $("#zone").on('change', function(){
            $("#sid").empty();
            $("#uid").empty();
            $("#cid").empty();
            var end = $(this).val();
            console.log(end);
            $.ajax({
                async : true,
                cache : false,
                url : 'api/getState.php',
                data:{
                    zone:end
                },
                type:'POST',
                success: function(result){
                    res = jQuery.parseJSON(result);
                    console.log(res);
                        $("#sid").append($('<option>',{
                            value: '',
                            text: "--Select--"
                        }));
                    for(var key in res){
                        //console.log(res[key].name);
                        $("#sid").append($('<option>',{
                            value: res[key].id,
                            text: res[key].name
                        }));
                    }
                    sortSelect("#sid","text","asc");
                }
            });
        });

        $("#sid").on('change', function(){
            $("#uid").empty();
            $("#cid").empty();
            var end = $(this).val();
            var zone = $("#zone").val();
            console.log(end);
            $.ajax({
                async : true,
                cache : false,
                url : 'api/getUniversities.php',
                data:{
                    state_id:end
                },
                type:'POST',
                success:function(result){
                    res = jQuery.parseJSON(result);
                    console.log(res);
                        $("#uid").append($('<option>',{
                            value: '',
                            text: "--Select--"
                        }));
                    for(var key in res){
                        //console.log(res[key].name);
                        $("#uid").append($('<option>',{
                            value: res[key].uni_id,
                            text: res[key].name
                        }));
                    }
                    sortSelect("#uid","text","asc");
                }
            });
        });

        $("#uid").on('change', function(){
            $("#cid").empty();
            var uid = $("#uid").val();
            console.log(uid);
            $.ajax({
                async : true,
                cache : false,
                url : 'api/getColgByUniversityId.php',
                data:{
                    uni_id:uid
                },
                type:'POST',
                success:function(result){
                    res = jQuery.parseJSON(result);
                    console.log(res);
                        $("#cid").append($('<option>',{
                            value: '',
                            text: "--All--"
                        }));
                    for(var key in res){
                        //console.log(res[key].name);
                        $("#cid").append($('<option>',{
                            value: res[key].cid,
                            text: res[key].name
                        }));
                    }
                    sortSelect("#uid","text","asc");
                }
            });
        });
        var value;
        $("#cid").on('change', function(){
            value = $(this).val();
        });

        $("#search").click (function(){
            window.location = "collegeDashboard.php?colg_id="+value;
        });
});