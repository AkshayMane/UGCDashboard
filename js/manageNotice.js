/**
 * Created by Franky Naidu on 3/31/2017.
 */

$(document).ready(function () {


    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        defaultDate: new Date(),
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        selectable: true,
        selectHelper: true,
        listDayFormat:true,
        allDayDefault:false,
        select: function(start, end) {

            $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
            $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
            $('#ModalAdd').modal('show');
        },
        eventRender: function(event, element) {
            element.bind('dblclick', function() {
                $('#ModalEdit #id').val(event.id);
                $('#ModalEdit #title').val(event.title);
                $('#ModalEdit #color').val(event.backgroundColor);
                $('#ModalEdit').modal('show');
            });
        },
        eventDrop: function(event, delta, revertFunc) { // If change of position
            edit(event);
        },
        eventResize:function(event,dayDelta,minuteDelta,revertFunc) { // If change of length
            edit(event);
        },
        events: "./api/getEvents.php"
    });


    function edit(event){
        var postdata = {
            "id": event.id,
            "title":event.title,
            "sdate":event.start.format('YYYY-MM-DD HH:mm:ss'),
            "edate": event.end ? event.end.format('YYYY-MM-DD HH:mm:ss'):event.start.format('YYYY-MM-DD HH:mm:ss')
        };

        $.ajax({
            url: './api/updateEvent.php',
            type: "POST",
            data: postdata,
            success: function(response) {
                if(response == 'Updated'){
                    alert('Event Saved');
                }else{
                    alert('Could not be saved. try again.');
                }
            }
        });
    }

    $('#ModalEditForm').submit(function(event){
        event.preventDefault();
        var form = $(this);
        data = form.serialize();
        console.log(data);
        $.ajax({
            url: './api/updateEvent.php',
            type: "POST",
            data:data,
            success: function(response) {
                if(response == 'Updated' || response == 'Deleted'){
                    $('#calendar').fullCalendar( 'refetchEvents' );
                    document.getElementById("ModalEditForm").reset();
                    $('#ModalEdit').modal('hide');
                }else{
                    alert(response);
                }
            }
        })
    });


    $('#ModalAddForm').submit(function(event){
        event.preventDefault();
        var form = $(this);
        data = form.serialize();
        console.log(data);
        $.ajax({
            url: './api/insertEvents.php',
            type: "POST",
            data:data,
            success: function(response) {
                if(response == 'Event Added'){
                    $('#calendar').fullCalendar( 'refetchEvents' );
                    document.getElementById("ModalAddForm").reset();
                    $('#ModalAdd').modal('hide');
                }else{
                    alert(response);
                }
            }
        })
    });



});