var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };


var main = {
                'affiliation' : 
                    {   
                        'P':'Permanent',
                        'T':'Temporary'
                    },
                'edu_level' :
                    {   
                        'H':'Higher Secondary',
                        'P':'PhD',
                        'M':'Masters',
                        'B':'Bachelors'
                    },
                'gov' : 
                    {
                        '1':'Government Insitute',
                        '0':'Non-Government Institute'
                    },
                'aided' :
                    {
                        'A' : 'Aided',
                        'U' : 'Un-aided',
                        '' : '-'
                    },
                '12f' :
                    {
                        '1' : 'Yes',
                        '0' : 'No'
                    },
                '12b' :
                    {
                        '1' : 'Yes',
                        '0' : 'No'
                    },
                'zone' :
                    {
                        'W' : 'West',
                        'N' : 'North',
                        'S' : 'South',
                        'E' : 'East',
                    }
            };