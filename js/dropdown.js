$(document).ready(function(){

    var sortSelect = function (select, attr, order) {
    if(attr === 'text'){
        if(order === 'asc'){
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
        }// end asc
        if(order === 'desc'){
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
        }// end desc
    }
    };

var data;
    if($("#cid").length != 0){
    	$.ajax({
    		async : true,
    		cache : false,
    		url : 'api/getColg.php',
    		data:data,
    		type:'POST',
    		success:function(result){
                res = jQuery.parseJSON(result);
                console.log(res);
                for(var key in res){
                	//console.log(res[key].name);
                	$("#cid").append($('<option>',{
                		value: res[key].cid,
                		text: res[key].name
                	}));
                }
                sortSelect("#cid","text","asc");
    		}
    	});
    }
    if($("#did").length != 0){
        $.ajax({
            async : true,
            cache : false,
            url : 'api/getDept.php',
            data:data,
            type:'POST',
            success:function(result){
                res = jQuery.parseJSON(result);
                console.log(res);
                for(var key in res){
                    //console.log(res[key].name);
                    $("#did").append($('<option>',{
                        value: res[key].did,
                        text: res[key].name
                    }));
                }
                sortSelect("#did","text","asc");
            }
        });
    }
	if($("#uni_id").length != 0 ){
        $("#zone").on('change', function(){
            $("#uni_id").empty();
            var end = $(this).val();
            console.log(end);
            $.ajax({
                async : true,
                cache : false,
                url : 'api/getUniversities.php',
                data:{
                    zone:end
                },
                type:'POST',
                success:function(result){
                    res = jQuery.parseJSON(result);
                    //console.log(res);
                    for(var key in res){
                        //console.log(res[key].name);
                        $("#uni_id").append($('<option>',{
                            value: res[key].uni_id,
                            text: res[key].name
                        }));
                    }
                    sortSelect("#uni_id","text","asc");
                }
            });
        });
    }
    
});
