$(document).ready(function(){    
    
    var inputBox = ['name','address','yoe','contact','pincode','colg_code','mail','password','state','affiliation','edu_level','gov','aided','12f','12b','naac'];
    var dropdownBoxUniversity = ['uni_id','zone'];
    var colg_id = window.getUrlParameter('colg_id');
    console.log(colg_id)
        $.ajax({
            async : true,
            cache : false,
            url:'api/getColgById.php',
            data:{
                collegeid : colg_id
            },
            type:'POST',
            success:function(result){
            res = jQuery.parseJSON(result);
            var college = res[0];
            var university = res[1];
            for(var key in inputBox){
                $("#"+inputBox[key]).val(college[inputBox[key]]);
            }
            for(var key in dropdownBoxUniversity){
                $("#"+dropdownBoxUniversity[key]).val(university[dropdownBoxUniversity[key]]);
            }
            $("#password").prop("disabled","true");            
            }
        });
        $( document ).ajaxError(function() {
            console.log("Error");
        });
        $( document ).ajaxSuccess(function() {
            console.log("success");
        });


        $("#colg_form").submit(function(event){
        console.log("Hello");
        event.preventDefault();
        var form = $(this);
        var data = form.serialize();
        console.log(data);
        $.ajax({
            async : true,
            cache : false,
            url:'api/updateColg.php',
            data:data,
            type:'POST',
            success:function(result){
        $("#alertBox").addClass("alert-success");
        $("#alertBox").css("display","block");
        $("#alertMsg").html(result);
            }
        });
        $( document ).ajaxError(function() {
            console.log("Error");
        });

    });

});