$(document).ready(function(){    
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };var main = {
                'affiliation' : 
                    {   
                        'P':'Permanent',
                        'T':'Temporary'
                    },
                'edu_level' :
                    {   
                        'H':'Higher Secondary',
                        'P':'PhD',
                        'M':'Masters',
                        'B':'Bachelors'
                    },
                'gov' : 
                    {
                        '1':'Government Insitute',
                        '0':'Non-Government Institute'
                    },
                'aided' :
                    {
                        'A' : 'Aided',
                        'U' : 'Un-aided',
                        '' : '-'
                    },
                '12f' :
                    {
                        '1' : 'Yes',
                        '0' : 'No'
                    },
                '12b' :
                    {
                        '1' : 'Yes',
                        '0' : 'No'
                    },
                'zone' :
                    {
                        'W' : 'West',
                        'N' : 'North',
                        'S' : 'South',
                        'E' : 'East',
                    }
            };
    var college = ['name','address','yoe','contact','pincode','colg_code','mail','state','naac'];
    var good = ['affiliation','edu_level','gov','aided','12f','12b'];
    var university = ['name','zone'];
    var student = ['total_stud','min_stud','min_prop'];
var colg_id = getUrlParameter('colg_id');
        $.ajax({
            async : true,
            cache : false,
            url:'api/getColgById.php',
            data:{
                collegeid : colg_id
            },
            type:'POST',
            success:function(result) {
                res = jQuery.parseJSON(result);
                if (res.error == "Invalid College ID") {
                    window.location = "college404.php";
                }
                else {
                        var collegeData = res[0];
                        var universityData = res[1];
                        var studentData = res[2];
                        // console.log(universityData);
                        // console.log(studentData);
                        for (var key in college) {
                            $("#" + college[key]).html(collegeData[college[key]]);
                        }
                        for (var key in student) {
                            $("#" + student[key]).html(studentData[student[key]]);
                        }
                        console.log(main['edu_level']['H']);
                        for (var key in good) {
                            $("#" + good[key]).html(main[good[key]][collegeData[good[key]]]);
                        }
                        $("#uname").html(universityData['name']);
                    }
            }
        });
        var projects = {
            'H' : 0,
            'S' : 0,
            'G' : 0,
            'R' : 0,
            'total' : 0
        };
        $.ajax({
            async:true,
            cache:false,
            url:'api/grantRelatedStats.php',
            type:'POST',
            data:{
                college_id:colg_id
            },
            success:function(result){
                var res = jQuery.parseJSON(result);
                console.log(res);
                for(var key in res){
                    projects[res[key]['type']] = parseInt(projects[res[key]['type']]) + parseInt(res[key]['count']);
                    projects['total'] = parseInt(projects['total']) + parseInt(res[key]['count']);
                }
                $("#totalH").html(projects['H']);
                $("#totalR").html(projects['R']);
                $("#totalS").html(projects['S']);
                $("#totalG").html(projects['G']);
                $("#totalGrants").html(projects['total']);
                console.log(projects);
                    var ctx=$("#budgetCanvas").get(0).getContext("2d");
                    var c=$("#grantCanvas").get(0).getContext("2d");
                    //var cr=$("#newcanvas").get(0).getContext("2d");
                    var data=[
                        {
                            value:projects['H'],
                            color:"#00a65a",
                            highlight:"#aed7a6",
                            label:"Hardware"
                        },
                        {
                            value:projects['S'],
                            color:"#f39c12",
                            highlight:"#f3c282",
                            label:"Software"
                        },
                        {
                            value:projects['G'],
                            color:"#3c8dbc",
                            highlight:"#acc6ee",
                            label:"Greenhouse"
                        },
                        {
                            value:projects['R'],
                            color:"#dd4b39",
                            highlight:"#dd9780",
                            label:"Research"
                        }
                    ];
                var pieChart =new Chart(ctx).Pie(data);
            }
        });

});