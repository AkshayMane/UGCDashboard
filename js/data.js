
                $(document).ready(function(){
                    var ctx=$("#budgetCanvas").get(0).getContext("2d");
                    var c=$("#grantCanvas").get(0).getContext("2d");
                    var un=$("#uniCanvas").get(0).getContext("2d");



                    var data_uni=[
                        {
                            value:8,
                            color:"#00a65a",
                            highlight:"#aed7a6",
                            label:"A"
                        },
                        {
                            value:10,
                            color:"#f39c12",
                            highlight:"#f3c282",
                            label:"B"
                        },
                        {
                            value:8,
                            color:"#3c8dbc",
                            highlight:"#acc6ee",
                            label:"C"
                        },
                        {
                            value:9,
                            color:"#dd4b39",
                            highlight:"#dd9780",
                            label:"D"
                        }
                    ];


                    var datab=[
                        {
                            value:8,
                            color:"#00a65a",
                            highlight:"#aed7a6",
                            label:"2011"
                        },
                        {
                            value:10,
                            color:"#f39c12",
                            highlight:"#f3c282",
                            label:"2012"
                        },
                        {
                            value:8,
                            color:"#3c8dbc",
                            highlight:"#acc6ee",
                            label:"2013"
                        },
                        {
                            value:9,
                            color:"#dd4b39",
                            highlight:"#dd9780",
                            label:"2014"
                        },
                        {
                            value:12,
                            color:"#777",
                            highlight:"#bebebe",
                            label:"2015"
                        }
                    ];
                    var pieChart =new Chart(c).Doughnut(datab);
                    var donut =new Chart(un).Doughnut(data_uni);
                });