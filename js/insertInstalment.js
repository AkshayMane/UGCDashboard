$(document).ready(function() {

	var project_id = window.getUrlParameter('project_id');

	$.ajax({
		async : true,
		cache:false,
		url:'api/getProjById.php',
		data:{
			project_id:project_id
		},
		type:'POST',
		success:function(result){
			var res = jQuery.parseJSON(result);
			console.log(res);
				$("#name").val(res[project_id]['name']);
				$("#college_name").val(res[project_id]['college']['name']);
		}
	});

	$("#formSubmit").submit(function(event){
        event.preventDefault();
		var pid = $('#pid').val();
		var amt = $('#amt').val();
		console.log(pid);
		console.log(amt);
		$.ajax({
			async:true,
			cache:false,
			url:'api/insertInstalment.php',
			type:'POST',
			data:{
				project_id:pid,
				amt:amt
			},
			success:function(result){
				var res = jQuery.parseJSON(result);
		        $("#alertBox").addClass("alert-success");
		        $("#alertBox").css("display","block");
		        $("#alertMsg").html(res);
			}

		});

	});

});