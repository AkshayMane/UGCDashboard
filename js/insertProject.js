$(document).ready(function(){
    $("#colg_form").on('submit',function(event){
        event.preventDefault();
        var form = $(this);
        form.ajaxSubmit(
            {
                beforeSend: function () {
                    $(".progress").show();
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    $(".progress-bar").width(percentComplete + '%');
                    $(".sr-only").html(percentComplete + '%');
                },
                success: function () {
                    $(".progress").hide();
                },
                complete: function (response) {
                    $("#alertBox").html(response.responseText);
                    $("#alertBox").addClass("alert-success");
                    $("#alertBox").css("display","block");
                    document.getElementById("colg_form").reset();
                }
            });

    });

});