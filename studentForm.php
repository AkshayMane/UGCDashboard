<?php
  require_once 'session.php';
  require_once 'head.php';
  require_once 'sidebar.php';
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
            <h1>
        Student Form
      </h1>
      <div class="alert alert-dismissible" id="alertBox" style="display: none" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> <span id="alertMsg"></span></h4>
      </div>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active"><bold>General Elements</bold></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
  
<div id="header">
          <!-- general form elements -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Student Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" id="student_form">
              <div class="box-body">
                <div class="form-group">
                  <label for="totalStrength">Total Strength</label>
                  <input name="total_stud" type="text" class="form-control" id="total_stud" placeholder="Enter Total Strength">
                </div>
                <div class="form-group">
                  <label for="minStrength">Minority Strength</label>
                  <input name="min_stud" type="text" class="form-control" id="min_stud" placeholder="Enter Minority Strength">
                </div>
                <div class="form-group">
                  <label for="minProportion">Minority Proportion</label>
                  <input name="min_proportion" type="text" class="form-control" id="min_proportion" placeholder="Enter Minority Proportion">
                </div>
                <div class="form-group">
                  <label for="minProportion">Current Year</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="totalStrength">SC Students</label>
                  <input name="sc_stud" type="text" class="form-control" id="total_stud" placeholder="Enter Total Strength">
                </div>
                <div class="form-group">
                  <label for="minStrength">ST Students</label>
                  <input name="st_stud" type="text" class="form-control" id="min_stud" placeholder="Enter Minority Strength">
                </div>
                <div class="form-group">
                  <label for="minProportion">NT Students</label>
                  <input name="nt_stud" type="text" class="form-control" id="min_proportion" placeholder="Enter Minority Proportion">
                </div>
                <div class="form-group">
                  <label for="minProportion">OBC Students</label>
                  <input name="obc_stud" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="totalStrength">Open Students</label>
                  <input name="open_stud" type="text" class="form-control" id="total_stud" placeholder="Enter Total Strength">
                </div>
                <div class="form-group">
                  <label for="minStrength">Male Students</label>
                  <input name="" type="text" class="form-control" id="min_stud" placeholder="Enter Minority Strength">
                </div>
                <div class="form-group">
                  <label for="minProportion">Female Students</label>
                  <input name="min_proportion" type="text" class="form-control" id="min_proportion" placeholder="Enter Minority Proportion">
                </div>
                <div class="form-group">
                  <label for="minProportion">Male Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Male Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Female Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Permanent Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Male Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Male Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Male Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Male Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
                <div class="form-group">
                  <label for="minProportion">Male Staff</label>
                  <input name="year" type="text" class="form-control" id="year" placeholder="Enter Current year">
                </div>
              </div>
              <?php
                if(isset($college_id) and $college_id != ''){
                  echo '<input name="cid" value="'.$college_id.'" style="display:none" />';
                }

              ?>
              <!-- /.box-body -->
              <div class="">
            <button type="submit" class="btn btn-lg btn-primary col-md-offset-5 col-md-2">Submit</button>
          </div>
            </form>
          </div>
          </div>
          </div>
          </div>
          </section>
          </div>
          <!-- /.box -->
<?php
  if(!isset($_GET['stud_id'])){
    ?>
      <script type="text/javascript" src="js/insertStudent.js"></script>
    <?php
  }
?>