<?php
  require_once 'head.php';
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div id="header"></div>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        UGC Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div id="map" class="img-responsive" style="width:1400px; height:450px;"></div>
      <br>


      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-bar-chart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"></span>
              <span class="info-box-number">Schemes under UGC</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>48 </h3>

              <p>UGC Kaushal</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a href="#" class="small-box-footer"> <i></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>39</h3>

              <p>B.Voc</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>41</h3>

              <p>Community Colleges</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a href="#" class="small-box-footer"> <i></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>79</h3>

              <p>Scheme 4</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a href="#" class="small-box-footer"> <i></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-olive"><i class="fa fa-graduation-cap"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"></span>
              <span class="info-box-number">Fellowships under UGC</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>3,502</h3>

              <p>Rajiv Gandhi</p>
            </div>
            <div class="icon">
              <i class="ion ion-compose"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>3,853</h3>

              <p>Maulana Azad</p>
            </div>
            <div class="icon">
              <i class="ion-compose"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>3,654</h3>

              <p>Swami Vivekanand</p>
            </div>
            <div class="icon">
              <i class="ion ion-compose"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>4,005</h3>

              <p>Ishan Uday</p>
            </div>
            <div class="icon">
              <i class="ion ion-compose"></i>
            </div>
            <a href="#" class="small-box-footer"> <i ></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->




    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Grant-related Stats (Since inception)</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Total Grants</th>
                  <th><span class="label label-success">Hardware</span></th>
                  <th><span class="label label-warning">Software</th>
                  <th><span class="label label-primary">Greenhouse</th>
                  <th><span class="label label-danger">Research</th>
                </tr>
                </tr>
                </thead>
                <tbody>
            
                <tr>
                  <td>1004</td>
                  <td>561</td>
                  <td>263</td>
                  <td>110</td>
                  <td>70</td>
                </tr>
             

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Grant-related Stats (Last Five Years)</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>

                </thead>
                <tbody>
                <tr>
                  <th>Year</th>
                  <th>Total Grants</th>
                  <th><span class="label label-success">Hardware</span></th>
                  <th><span class="label label-warning">Software</th>
                  <th><span class="label label-primary">Greenhouse</th>
                  <th><span class="label label-danger">Research</th>
                  <th>Funds allocated</th>
                  <th>Received</th>
                </tr>
                <tr>
                  <td>2011</td>
                  <td>8</td>
                  <td>1</td>
                  <td>5</td>
                  <td>2</td>
                  <td>0</td>
                  <td>2,00,000</td>
                  <td>80,000</td>
                </tr>

                <tr>
                  <td>2012</td>
                  <td>10</td>
                  <td>4</td>
                  <td>3</td>
                  <td>1</td>
                  <td>2</td>
                  <td>2,00,000</td>
                  <td>80,000</td>
                </tr>

                <tr>
                  <td>2013</td>
                  <td>8</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2,00,000</td>
                  <td>90,000</td>
                </tr>

                <tr>
                  <td>2014</td>
                  <td>9</td>
                  <td>3</td>
                  <td>3</td>
                  <td>2</td>
                  <td>1</td>
                  <td>2,00,000</td>
                  <td>1,00,000</td>
                </tr>

                <tr>
                  <td>2015</td>
                  <td>12</td>
                  <td>6</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2</td>
                  <td>2,00,000</td>
                  <td>1,20,000</td>
                </tr>


                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>

          
          <!-- /.box -->

        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>










      <!-- Main row -->

          <!-- /.box (chat box) -->

          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">To Do List</h3>

              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="todo-list">
                <li>
                  <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <!-- checkbox -->
                  <input type="checkbox" value="">
                  <!-- todo text -->
                  <span class="text">Design a nice theme</span>
                  <!-- Emphasis label -->
                  <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Make the theme responsive</span>
                  <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Check your messages and notifications</span>
                  <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
            </div>
          </div>
          <!-- /.box -->

          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Quick Email</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              <form action="#" method="post">
                <div class="form-group">
                  <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" placeholder="Subject">
                </div>
                <div>
                  <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              </form>
            </div>
            <div class="box-footer clearfix">
              <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>

        </section>
        </span>
        </th></span></th></span></th></tr></tbody></table></div>
        <div id="footer">
        </div>
        <!-- /.Left col -->
<!--Map-->
<script type="text/javascript"
        src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDMq0txLAQD501dvmvA9Nsi7d1qkIAKyXE">
</script>
<script src="assets/markerCluster.js">
</script>
<!--
<script>
    function initMap() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        // Display a map on the web page
        map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
        map.setTilt(50);

        // Multiple markers location, latitude, and longitude
        var markers = [
            ['Fr. Conceicao Rodrigues College of Engineering', 19.044742,72.820062],
            ['Fr. C. Rodrigues Institute of Technology', 19.075527,72.991806],
            ['Indian Institute of Technology Bombay', 19.133430,72.913268],
            ['K. J. Somaiya College of Engineering', 19.073085,72.899822],
            ['College of Engineering, Pune', 18.529399, 73.856563]
        ];

        // Info window content
        var infoWindowContent = [
            ['<div class="info_content">' +
            '<h3>Fr. Conceicao Rodrigues College of Engineering</h3>' +
            '<p>Father Angels Ashram Road, Bandra West, Mumbai, Maharashtra 400050</p>' + '</div>'],
            ['<div class="info_content">' +
            '<h3>Fr. C. Rodrigues Institute of Technology</h3>' +
            '<p>Fr. Conceicao Rodrigues Institute of Technology (Fr. CRIT),Agnel Technical Education ComplexSector 9-A, Vashi</p>' +
            '</div>'],
            ['<div class="info_content">' +
            '<h3>Indian Institute of Technology Bombay</h3>' +
            '<p>IIT Area, Powai, Mumbai, Maharashtra 400076</p>' +
            '</div>'],
            ['<div class="info_content">' +
            '<h3>K. J. Somaiya College of Engineering</h3>' +
            '<p>Vidyanagar, Vidya Vihar East, Vidyavihar, Mumbai, Maharashtra 400077</p>' + '</div>'],
            ['<div class="info_content">' +
            '<h3>College of Engineering, Pune</h3>' +
            '<p>Wellesley Road, Shivajinagar, Pune, Maharashtra 411005</p>' + '</div>']
        ];

        // Add multiple markers to map
        var infoWindow = new google.maps.InfoWindow(), marker, i;

        // Place each marker on the map
        for( i = 0; i < markers.length; i++ ) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0]
            });



            google.maps.event.addListener(marker, 'click', function() {

                window.open('http://www.google.com','_blank');
                // infoWindow.open('http:///www.google.com');
            });

            // Add info window to marker
            google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                return function() {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Center the map to fit all markers on the screen
            map.fitBounds(bounds);
        }

        // Set zoom level
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(9);
            google.maps.event.removeListener(boundsListener);
        });

    }
    // Load initialize function
    google.maps.event.addDomListener(window, 'load', initMap);
</script>

<script src="js/manageNotice.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php")

    });
</script>
-->

<script>

    function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 3,
            center: {lat: 19.044742, lng: 72.820062}
        });

        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
            return new google.maps.Marker({
                position: location,
                label: labels[i % labels.length]
            });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'http://vignette2.wikia.nocookie.net/joke-battles/images/a/a1/Dot-com-clipart-dot-clipart-8e5a5a7ed9284c00a7bc45f2ad1ea8e8VPJ1OJ.png/revision/latest?cb=20151203020909'});
    }
    var locations = [
        {lat: 19.044742, lng: 72.820062, name:'Fr. Conceicao Rodrigues College of Engineering'},
        {lat: 19.075527, lng: 72.991806, name:'Fr. C. Rodrigues Institute of Technology'},
        {lat: 19.133430, lng: 72.913268},
        {lat: 19.073085, lng: 72.899822},
        {lat: 18.529399, lng: 73.856563},
        {lat: -34.671264, lng: 150.863657},
        {lat: -37.819616, lng: 144.968119},
        {lat: -38.330766, lng: 144.695692}
    ]

    /*google.maps.event.addListener(marker, 'click', function() {

        window.open('http://www.google.com');
        // infoWindow.open('http:///www.google.com');
    });*/

</script>





 
</body>
</html>
