<?php

if(isset($_GET['colg_id'])){
	$college_id = $_GET['colg_id'];
}else{
	header("Location:collegeApproved.php ");
  exit();
}
require_once 'api/connection.php';
require_once 'mail.php';
  if($_POST){
    if(isset($_POST['yes'])){
      $query = "UPDATE college SET isapproved = 1 WHERE cid = ".$college_id;
      if($con->query($query)){
        $query = "select * FROM college WHERE cid = ".$college_id;
        $result = $con->query($query);
        $row = $result->fetch_assoc();
        $mailTo = $row['mail'];
        $name = $row['name'];
        $mailTo = $row['mail'];
        $subject = "College Approved by UGC";
        $content = file_get_contents('email_contents/approval.html');
        $content = str_replace("|*College_Name*|", $row['name'], $content);
        $_SESSION['colg_approved'] = true;
        phpMail($mailTo,$name,$subject,$content);
      }
    }
    
    header("Location: collegeApproved.php");
    exit();
    
  }
require_once 'head.php';
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div id="header"></div>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      	<center>You want to approve...?</center>  
      </h1>
     </section>
     <div class="content">
     <center>
     	<form method="POST">
     	<button name="yes" type="submit" class="btn btn-success btn-lg">Yes</button>
     	<button name="no"  type="submit" class="btn btn-danger btn-lg">No</button>
     	</form>
     </center>
     </div>
     </div>
     <div class="footer">
     </div>
     <script type="text/javascript">
        $(function () {
            /*Load The header*/
            $('#header').load("header.php");
            $('#footer').load("footer.php")

        });
    </script>
