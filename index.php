
    <?php

        session_start();
        if(isset($_SESSION['type'])){
            if(isset($_SERVEr['HTTP_REFERER'])){
                $url = $_SERVER['HTTP_REFERER'];
                header("Location: $url");
            }
        }

    	require_once 'head.php';
    ?>

    <style>
        body{
            background-color: #ECF0F5;
        }
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 50%;
            margin: auto;
        }
        .carousel-inner{
            background-color: #3C8DBC;
        }
        #mapCanvas {
            width: 100%;
            height: 100%;
        }
        .carousel,.item,.active{height:100%;}
        .carousel-inner{height:100%;}

    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    <header class="main-header">
        <nav class="navbar navbar-fixed-top">
            <div class="" style="margin: 5px;padding-left: 40px;padding-right: 20px;">
                <img src="img/logo.png" class="pull-left " style="padding: 5px;" width="50px" height="48px">

                <div class="navbar-header">
                    <a href="#" class="navbar-brand " ><b>UGCHome</b></a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                
                    <ul class="nav navbar-nav pull-right">
                        <?php
                            if(isset($_SESSION["login"])){
                                if($_SESSION["login"])
                                    echo '<li><a href="logout.php"><b>Logout</b></a>';

                            }
                            else{
                                echo '<li> <a href="collegeForm.php"><b>College Registration</b></a></li>';
                    
                                echo '<li><a href="login.php"><b>Login</b></a></li>';
                            }

                        ?>
                    </ul>
                </div>

                <!-- /.navbar-collapse -->

                <!-- /.navbar-custom-menu -->
            </div>
        </nav>
    </header>


    <!-- Full Width Column -->
    <div class="content-wrapper">
        <div class="" style="margin-top: 50px;margin-bottom: 50px;">
            <!-- Content Header (Page header)
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                </ol>
            </section>

            <!-- Main content -->
            <section class="content-header col-md-10 col-md-offset-1">


<!--
                <div class="">

                    <div  id = "ride"class="carousel slide" data-ride="carousel" data-interval = "2000">
                        <!-- Indicators
                        <ol class="carousel-indicators">
                            <li data-target="#ride" data-slide-to="0"></li>
                            <li data-target="#ride" data-slide-to="1"></li>
                            <li data-target="#ride" data-slide-to="2"></li>
                            <li data-target="#ride" data-slide-to="3"  class="active"></li>
                            <li data-target="#ride" data-slide-to="4"></li>
                        </ol>

                        <!-- Wrapper for slides
                        <div class="carousel-inner" role="listbox">
                            <div class="item"><img src="dist/img/11.png" width="100%" height="100%" class = "img-responsive"alt="CSI">
                                <div class="carousel-caption animated lanimation slideRight " >
                                    <blockquote>
                                        <p>India has long been an exporter of talent to tech companies... But it is India that's now undergoing its own revolution.<p>
                                        <footer>Sundar Pichai</footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/img/2.jpg" width="100%" height="100%" class = "img-responsive" alt="CSI">
                                <div class="carousel-caption fadeInUp animated lanimation ">
                                    <blockquote>
                                        <p>"All of my friends who have younger siblings who are going to college or high school - my number one piece of advice is: You should learn how to program."</p>
                                        <footer>Mark Zuckerberg</footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="item active ">
                                <img src="dist/img/3.jpg" width="100%" height="100%" class = "img-responsive" alt="CSI">
                                <div class="carousel-caption animated slideRight lanimation  ">
                                    <blockquote>
                                        <p>"Everything is going to be connected to cloud and data.All of this will be mediated by software."</p>
                                        <footer>Satya Nadella</footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/img/11.png" width="100%" height="100%" class = "img-responsive" alt="CSI">
                                <div class="carousel-caption animated slideRight lanimation ">
                                    <blockquote>
                                        <p>"Computers themselves, and software yet to be developed, will revolutionize the way we learn."</p>
                                        <footer>Steve Jobs</footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/img/2.jpg" width="100%" height="100%" class = "img-responsive" alt="CSI">
                                <div class="carousel-caption animated slideRight lanimation ">
                                    <blockquote>
                                        <p>"Don’t compare yourself with anyone in this world. If you do so, you are insulting yourself.”</p>
                                        <footer>Bill Gates</footer>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                -->



                <div class="box box-primary">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel" >
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <div class="item active ">
                                <img src="img/1.jpg" alt="UGC" class="img-responsive">
                                <div class="carousel-caption">
                                </div>
                            </div>

                            <div class="item">
                                <img src="img/2.jpg" alt="UGC" class="img-responsive">
                                <div class="carousel-caption">
                                </div>
                            </div>

                            <div class="item">
                                <img src="img/3.jpg" alt="UGC" class="img-responsive">
                                <div class="carousel-caption">
                                </div>
                            </div>

                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

                            <!-- /.box -->



                <!--
                <div class="box">
                    <iframe
                            width="1100"
                            height="450"
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDtjtF-aYVrzUJKIucAX4W2L28iKotaCAI
                                &q=,India" allowfullscreen>
                    </iframe>
                </div>
                -->
                <div class="box">
                         <div id="mapCanvas" class="img-responsive" style="width:1210px; height:450px;"></div>
                </div>
            </section>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="container">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2017 <a href="#"> University Grants Commission New Delhi, India</a>.</strong> All rights
            reserved.
        </div>
        <!-- /.container -->
    </footer>
</div>
<!-- ./wrapper -->

<script type="text/javascript"
        src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDMq0txLAQD501dvmvA9Nsi7d1qkIAKyXE&sensor=false">
</script>
<script>
    function initMap() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        // Display a map on the web page
        map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
        map.setTilt(50);

        // Multiple markers location, latitude, and longitude
        var markers = [
            ['Fr. Conceicao Rodrigues College of Engineering', 19.044742,72.820062],
            ['Fr. C. Rodrigues Institute of Technology', 19.075527,72.991806],
            ['Indian Institute of Technology Bombay', 19.133430,72.913268],
            ['K. J. Somaiya College of Engineering', 19.073085,72.899822],
            ['College of Engineering, Pune', 18.529399, 73.856563]
        ];

        // Info window content
        var infoWindowContent = [
            ['<div class="info_content">' +
            '<h3>Fr. Conceicao Rodrigues College of Engineering</h3>' +
            '<p>Father Angels Ashram Road, Bandra West, Mumbai, Maharashtra 400050</p>' + '</div>'],
            ['<div class="info_content">' +
            '<h3>Fr. C. Rodrigues Institute of Technology</h3>' +
            '<p>Fr. Conceicao Rodrigues Institute of Technology (Fr. CRIT),Agnel Technical Education ComplexSector 9-A, Vashi</p>' +
            '</div>'],
            ['<div class="info_content">' +
            '<h3>Indian Institute of Technology Bombay</h3>' +
            '<p>IIT Area, Powai, Mumbai, Maharashtra 400076</p>' +
            '</div>'],
            ['<div class="info_content">' +
            '<h3>K. J. Somaiya College of Engineering</h3>' +
            '<p>Vidyanagar, Vidya Vihar East, Vidyavihar, Mumbai, Maharashtra 400077</p>' + '</div>'],
            ['<div class="info_content">' +
            '<h3>College of Engineering, Pune</h3>' +
            '<p>Wellesley Road, Shivajinagar, Pune, Maharashtra 411005</p>' + '</div>']
        ];

        // Add multiple markers to map
        var infoWindow = new google.maps.InfoWindow(), marker, i;

        // Place each marker on the map
        for( i = 0; i < markers.length; i++ ) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0]
            });



            google.maps.event.addListener(marker, 'click', function() {

                window.open('index.html','_blank');
                // infoWindow.open('http:///www.google.com');
            });

            // Add info window to marker
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Center the map to fit all markers on the screen
            map.fitBounds(bounds);
        }

        // Set zoom level
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(9);
            google.maps.event.removeListener(boundsListener);
        });

    }
    // Load initialize function
    google.maps.event.addDomListener(window, 'load', initMap);
</script>

<!--End of Tawk.to Script-->

</body>
</html>
