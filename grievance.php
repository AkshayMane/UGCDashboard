
<?php
require_once 'head.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div id="header"></div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                File Grievance
                <small>UGC Academic Calendar</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">File Grievance</li>
            </ol>
        </section>
        <!--Main Content-->
        <section class="content">
            <div class="row">
                <!-- /.col -->
                <div class="col-sm-8 col-sm-offset-2" style="padding-top:10px;">
                    <div class="box box-primary">
                        <div class="box-body ">
                            <!-- THE CALENDAR -->
                            <div id="calendar" class="block-center">
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>

    </div>
    <!-- /.content-wrapper -->
    <!--Footer-->
    <div id="footer">

    </div>

</div>
<!-- ./wrapper -->
<script src="js/displayCalendar.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php")

    });
</script>
</body>
</html>
