<?php?>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.8
        </div>
        <strong>Copyright &copy; 2017-2018 <a href="http://akshaymane.in">Pseudo_Prime</a>.</strong> All rights
        reserved.
    </footer>
