<?php
    require_once 'head.php';
?>
<![endif]-->


<body>



<!--start of login box-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <div class="login-panel panel " style="margin-top: 20%">
                    <div class="panel-heading">
                        <h3 class="text-center"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp;&nbsp;UGC Login</h3>
                    </div>
                    <div class="panel-body">

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <form class ="form-horizontal" role="form" id="loginform">
                                 <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="username" type="text" class="form-control" name="username" value="" placeholder="username">
                                 </div>
                                 <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="password" type="password" class="form-control" name="password" placeholder="password">
                                 </div>
                                 <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                        <select id="type" class="form-control" name="type" >
                                        <option value="ugc">UGC</option>
                                        <option value="university">University</option>
                                        <option value="college">College</option>
                                        </select>
                                 </div>
                                <div>
                                    <button type="submit" class="btn btn-primary" aria-label="Left Align" id="submitbtn" name="submit" value="submit">Login
                                        <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
                                    </button>



                                </div>
                     </div>
                    <!-- Change this to a button or input when using this as a form -->
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</section><!--end of login box-->




<script src="js/login.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</body>
<style type="text/css">
    body {
        background-image: url("img/background/login.jpg");
        background-repeat: no-repeat;
        background-size:100% 200%   ;
    }
    .login-panel{
        background : rgba(0,0,0,0.7);
        color: white;
    }
    .panel{
        border:1px solid #636b6f;
    }
#tit{
    font-size:28px;font-weight: 300;font-style: normal; font-family:caecilia,Times,serif;
}
</style>

</html>
