<?php
require_once 'head.php';
require_once 'api/connection.php';
$cid = $_SESSION["cid"];
if(!isset($_GET["proj_id"]))
    header("Location: viewProjects.php");
$pid = $_GET["proj_id"];
$sql = "select * from project where cid = $cid and pid = $pid and status like 'A'";
if($result=$con->query($sql)){
if ($result->num_rows < 0) {
    header("Location: viewProjects.php");
}
$row = $result->fetch_assoc();
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div id="header"></div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                UPLOAD BILL
            </h1>
            <div class="alert alert-dismissible" id="alertBox" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> <span id="alertMsg"></span></h4>
            </div>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">
                    <bold>General Elements</bold>
                </li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <form role="form" id="colg_form" enctype="multipart/form-data" action="api/insertBill.php"
                          method="POST">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Submit Bill</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Project Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder='<?php echo $row["name"] ?>' disabled>
                                </div>


                                <div class="form-group">
                                    <label for="bill">Upload your Final Bill</label>
                                    <input type="file" id="bill" name="bill">
                                </div>

                                <div class="form-group">
                                    <label>Amount Received</label>
                                    <input type="text" class="form-control" name="amt" id="amt"
                                           placeholder="Enter Amount Received ">
                                </div>


                                <div class="progress" style="display: none">
                                    <div class="progress-bar progress-bar-success progress-bar-striped"
                                         role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                                        <span class="sr-only"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (isset($_GET['proj_id']) and $_GET['proj_id'] != '') {
                            echo '<input style="display:none" name="project_id" value="' . $_GET['proj_id'] . '" />';
                            echo '<input style="display:none" name="cid" value="'.$cid.'"/>';
                        }
                    }
                        ?>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>
    <div id="footer"></div>


    <script type="text/javascript" src="js/dropdown.js"></script>
    <script src="assets/jquery-form/dist/jquery.form.min.js" ></script>
    <script src="js/insertBill.js" ></script>
    <script type="text/javascript">
        $(function () {
            /*Load The header*/
            $('#header').load("header.php");
            $('#footer').load("footer.php")

        });
    </script>
</body>
</html>
