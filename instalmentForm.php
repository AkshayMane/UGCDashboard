<?php
 
  require_once 'head.php';
  

?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper"> 
<div id="header">
</div>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Installment Form Elements
        <small>Preview</small>
      </h1>

      <div class="alert alert-dismissible" id="alertBox" style="display: none" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> <span id="alertMsg"></span></h4>
      </div>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Advanced Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-md-4">Project Name : </label>
                <div class="col-md-8">
                  <input type="text" name="name" id="name" class="form-control "  disabled>
                </div>
               </div>
              <div class="form-group">
                <label class="control-label col-md-4">By College  : </label>
                <div class="col-md-8">
                  <input type="text" name="name" id="college_name" class="form-control "  disabled>
                </div>
              </div>

              <!-- /.form-group -->
              <!-- /.form-group -->
            <!-- /.col -->
            <form type="POST" id="formSubmit">
            <?php
              if(isset($_GET['project_id'])){
                echo '<input type="text" value="'.$_GET['project_id'].'" id="pid" name="pid" style="display:none"/>';
              }
            ?>
            <div class="form-group">
                  <label for="install-amt" class="control-label col-md-4">Installment Amount :</label>
                  <div class="col-md-8">
                  <input type="text" name="amt" class="form-control " id="amt" placeholder="Enter the Installment Amount">
                </div>
                </div>

            </div>
              <!-- /.form-group -->
             
              <!-- /.form-group -->
              <div class="">
                <button type="submit" class="btn btn-lg btn-primary col-md-offset-5 col-md-2">Submit</button>
              </div>
	</div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        </form>
        <!-- /.box-body -->
            </div>
    </section>
</div>


<div id="footer"></div>
<script type="text/javascript" src="js/insertInstalment.js"></script>
<script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php")

    });
</script>

</div>
</body>
</html>
