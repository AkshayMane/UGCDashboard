<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 4/2/2017
 * Time: 9:10 AM
 */

require_once 'connection.php';
$sql = "SELECT title, DATE( startdate ) AS startdate, DATE( enddate ) AS enddate FROM  `events` WHERE DATE( startdate ) >= CURDATE( )  order by startdate limit 5";
$result = $con->query($sql);
$data =array();
if($result->num_rows > 0){
    while($row=$result->fetch_assoc()){
        $temp["title"]=$row["title"];
        $temp["start"]=$row["startdate"];
        $temp["end"]=$row["enddate"];
        array_push($data,$temp);
    }
}
else{
  $data =  array("error"=>"No Remainder");
}
echo json_encode($data);