<?php

require_once 'connection.php';

$sql = "SELECT * FROM department";

$result = $con->query($sql);
$data = array();
if($result->num_rows > 0){
	while($row = $result->fetch_assoc()){
		$data[$row['did']] = $row;
	}
}

echo json_encode($data);

