<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/25/2017
 * Time: 10:42 AM
 */
require ("connection.php");
if(isset($_POST)){
  $flag = true;
  $msg = "Please enter ";
  foreach ($collegeFields as $column => $value) {
    if(isset($_POST[$column]) && $_POST[$column] != ''){
      $collegeFields[$column]["value"] = $con->real_escape_string($_POST[$column]);
    }else{
      if(!$flag)
        $msg .= ", ";
      $flag = false;
      $msg .= $collegeFields[$column]["msg"];
    }
  }
  //var_dump($collegeFields);
  /*
    $colname = $con->real_escape_string($_POST["name"]);
    $address = $con->real_escape_string($_POST["address"]);
    $yoe = $con->real_escape_string($_POST["yoe"]);
    $pincode = $con->real_escape_string($_POST["pincode"]);
    $aff =$con->real_escape_string($_POST["affiliation"]);
    $level = strtoupper($con->real_escape_string($_POST["edu_level"]));
    $gov = $con->real_escape_string($_POST["gov"]) ? true : false;
    $aided = $con->real_escape_string($_POST["aided"]) ? true :false;
    $tb = $con->real_escape_string($_POST["12b"]);
    $tf = $con->real_escape_string($_POST["12f"]);
    $naac = $con->real_escape_string($_POST["naac"]);
    $state = $con->real_escape_string($_POST["state"]);
    $contact = $con->real_escape_string($_POST["contact"]);
    $mail = $con->real_escape_string($_POST["mail"]);
    $unid = $con->real_escape_string($_POST["uni_id"]);
    $code = $con->real_escape_string($_POST["colg_code"]);
    $password = md5($con->real_escape_string($_POST["password"]));
    */
    $comma = true;
    $target_dir='../ugc/';
    $sql = "INSERT INTO college (";
    foreach ($collegeFields as $column => $value) {
      if(!$comma){
        $sql .= " , ";
      }
      $comma = false;
      $sql .= "`$column`";
    }
    $sql .= ") VALUES (";
    $comma = true;
    foreach ($collegeFields as $column => $value) {
      if(!$comma){
        $sql .= " , ";
      }
      $comma = false;
      $sql .= "'".$collegeFields[$column]["value"]."'";
    }
    $sql .= ")"; 


    //$sql = "INSERT INTO `college`(`name`, `address`, `yoe`, `affiliation`, `edu_level`, `gov`, `aided`, `12f`, `12b`, `naac`, `state`, `contact`, `mail`, `uni_id`, `colg_code`, `password`, `pincode`) VALUES ('$colname','$address','$yoe','$aff','$level',$gov,'$aided','$tb','$tf','$naac','$state','$contact','$mail',$unid,$code,'$password', $pincode)";

    if($flag){
     if ($con->query($sql)){

         $cid = $con->insert_id;
         if(mkdir($target_dir."$cid"))
              echo "College Inserted";
     }
     else{
         echo $con->error;
     }
    }else{
      echo $msg;
    }

}
else{
    echo "Try Later";
}