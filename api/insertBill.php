<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/25/2017
 * Time: 2:11 PM
 */

require ("connection.php");

if(isset($_POST) && isset($_FILES["bill"])){
    $flag = true;
    $msg = "Please enter ";
    $target_dir = "../ugc/";
    $filename = $_FILES["bill"]["name"];
    $file = $_FILES["bill"]["tmp_name"];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $allowedext = array("pdf");
    $msg="";
    if(!in_array($ext,$allowedext)){
        echo  $msg .= "Extension not allowed, please upload .pdf format";
        return;
    }

    if(isset($_POST)){
            $amt = $con->real_escape_string($_POST["amt"]);
            $pid =$con->real_escape_string($_POST["project_id"]);
            $cid =$con->real_escape_string($_POST["cid"]);

            if (!file_exists($target_dir."$cid/"))
                mkdir($target_dir."$cid");
            if(!file_exists( $target_dir."$cid/"."$pid"))
                mkdir($target_dir."$cid/"."$pid");
            mkdir($target_dir."$cid/"."$pid/bill");
            $target_dir .= "$cid/"."$pid/bill/".$filename;
            if(move_uploaded_file($file,$target_dir)){
                $sql = "INSERT INTO `bills`(`pid`, `billname`, `amt_rec`) VALUES ($pid,'$filename',$amt)";
                if($con->query($sql)){
                        echo "Bill Submitted Successfully";
                }
                else{
                    echo $con->error;
                }
            }
            else{
                echo "Bill Not Submitted";
            }
        }
        else{
            echo "Entered else";
        }
}