<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/25/2017
 * Time: 2:11 PM
 */

require ("connection.php");

if(isset($_POST) && isset($_FILES["proposal"])){
  $flag = true;
  $msg = "Please enter ";
    $target_dir = "../ugc/";
    $filename = $_FILES["proposal"]["name"];
    $file = $_FILES["proposal"]["tmp_name"];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $allowedext = array("pdf");
    $msg="";
    if(!in_array($ext,$allowedext)){
        echo  $msg .= "Extension not allowed, please upload .pdf format";
        return;
    }
  foreach ($projectFields as $column => $value) {
    if(isset($_POST[$column]) && $_POST[$column] != ''){
      $projectFields[$column]["value"] = $con->real_escape_string($_POST[$column]);
    }else{
      if(!$flag)
        $msg .= ", ";
      $flag = false;
      $msg .= $projectFields[$column]["msg"];
    }
  }


$comma = true;
    $sql = "INSERT INTO project (";
    foreach ($projectFields as $column => $value) {
      if(!$comma){
        $sql .= " , ";
      }
      $comma = false;
      $sql .= "`$column`";
    }
    $sql .= ") VALUES (";
    $comma = true;
    foreach ($projectFields as $column => $value) {
      if(!$comma){
        $sql .= " , ";
      }
      $comma = false;
      $sql .= "'".$projectFields[$column]["value"]."'";
    }
    $sql .= ")";

/*if(isset($_POST)){
    $name = $con->real_escape_string($_POST["name"]);
    $cid = 1; //using seesion we can take the data;
    $title =$con->real_escape_string($_POST["title"]);
    $level = strtoupper($con->real_escape_string($_POST["edulevel"]));
    $year = $con->real_escape_string($_POST["year"]);
    $type = $con->real_escape_string($_POST["type"]);
    $leadername = $con->real_escape_string($_POST["leadername"]);
    $contact = $con->real_escape_string($_POST["contact"]);
    $mail = $con->real_escape_string($_POST["mail"]);


    $sql = "INSERT INTO `ugc`.`project` ( `name`, `cid`, `title`, `year`, `deptid`, `edu_level`, `type`,  `leader_name`, `leader_phone`, `leader_mail`,`filename`) VALUES ('$name',$cid,'$title',$year,'$dept','$level','$type','$leadername','$contact','$mail',$filename)";
*/
    if($flag){
        if($file != "" && isset($file)){
            if($con->query($sql))
            {
                $pid = $con->insert_id;
                $cid = $projectFields["cid"]["value"];
                if (!file_exists($target_dir."$cid/"))
                    mkdir($target_dir."$cid");
                if(!file_exists( $target_dir."$cid/"."$pid"))
                    mkdir($target_dir."$cid/"."$pid");
                mkdir($target_dir."$cid/"."$pid/proposal");
                $target_dir .= "$cid/"."$pid/proposal/".$filename;
                if(move_uploaded_file($file,$target_dir)){
                    $msg = "Project Proposal SucessFully Submited";
                    echo $msg;
                }
            }
            else{
                echo $con->error;
            }
        }else{
            echo "Entered else";
        }
    }
    else{
        echo $msg;
    }
}