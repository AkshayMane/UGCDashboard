<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/25/2017
 * Time: 10:42 AM
 */
require ("connection.php");
if(isset($_POST) and isset($_POST['colg_id'])){
  $colg_id = $_POST["colg_id"];
  $flag = true;
  $msg = "Please enter ";
  foreach ($collegeFields as $column => $value) {
    if($column != "password"){
        if(isset($_POST[$column]) && $_POST[$column] != ''){
          $collegeFields[$column]["value"] = $con->real_escape_string($_POST[$column]);
        }else{
          if(!$flag)
            $msg .= ", ";
          $flag = false;
          $msg .= $collegeFields[$column]["msg"];
        }
    }
}
if($flag){
    $comma = true;
        $sql = "UPDATE college SET ";
        foreach ($collegeFields as $column => $value) {
            if($column != "password"){
                if(!$comma){
                    $sql .= " , ";
                }
                $comma = false;
                $sql .= $column." = '".$collegeFields[$column]["value"]."'";
            }
        }
        $sql .= "WHERE cid = $colg_id";

    /*if(isset($_POST)){
        $colgid = $con->real_escape_string($_POST["colgid"]);
        $colname = $con->real_escape_string($_POST["name"]);
        $address = $con->real_escape_string($_POST["address"]);
        $yoe = $con->real_escape_string($_POST["yoe"]);
        $aff =$con->real_escape_string($_POST["aff"]);
        $level = strtoupper($con->real_escape_string($_POST["edulevel"]));
        $gov = $con->real_escape_string($_POST["gov"]) ? true : false;
        $aided = $con->real_escape_string($_POST["aided"]) ? true :false;
        $tb = $con->real_escape_string($_POST["tb"]);
        $tf = $con->real_escape_string($_POST["tf"]);
        $naac = $con->real_escape_string($_POST["naac"]);
        $state = $con->real_escape_string($_POST["state"]);
        $contact = $con->real_escape_string($_POST["contact"]);
        $mail = $con->real_escape_string($_POST["mail"]);
        $unid = $con->real_escape_string($_POST["universityId"]);
        $code = $con->real_escape_string($_POST["code"]);
        $password = md5($con->real_escape_string($_POST["password"]));
        $approved =  $con->real_escape_string($_POST["approved"]);


        $sql ="UPDATE `college` SET  name = '$colname',address = '$address',yoe = '$yoe',affiliation = '$aff',edu_level = '$level',gov='$gov',aided='$aided',12b=$tb,12f=$tf,naac='$naac',state='$state',contact='$contact',mail='$mail',uni_id='$unid',colg_code='$code',password='$password',approved = $approved where cid = $colgid";
        */
        if ($con->query($sql)){
            echo "College Data Updated";
        }
        else{
            echo $con->error;
        }
    }else{
        echo $msg;
    }

}
else{
    echo "Try Later";
}