<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/25/2017
 * Time: 10:42 AM
 */


require("connection.php");

if(isset($_POST)){

    $name = $con->real_escape_string($_POST["name"]);
    $zone= strtoupper($con->real_escape_string($_POST["zone"]));
    $code =  $con->real_escape_string($_POST["code"]);;

    if(!in_array($zone,array("N","S","E","W"))){
        echo json_encode(array("error"=>"Invalid Zone"));
        return;
    }
    $sql="INSERT INTO `university`(`name`, `zone`, `uni_code`) VALUES ('$name','$zone','$code')";
    if($con->query($sql))
        echo "Data Inserted";
    else
    {
        echo  "Data Not Inserted";
    }
}
else{
    echo "Try Later";
}