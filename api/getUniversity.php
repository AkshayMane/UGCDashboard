<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/25/2017
 * Time: 10:28 AM
 */

require ("connection.php");


if(isset($_POST["zone"])){
    $data = array();
    $zone = strtoupper($con->real_escape_string($_POST["zone"]));
    $sql = "select * from university where zone like '$zone'  ";
    if(!in_array($zone,array("N","S","E","W"))){
        echo json_encode(array("error"=>"Invalid Zone"));
        return;
    }

    $result = $con->query($sql);
    if($result->num_rows > 0){

        while($row = $result->fetch_assoc()){
            $temp["universityid"] = $row["uni_id"];
            $temp["name"] = $row["name"];
            $temp["code"] = $row["uni_code"];
            array_push($data,$temp);

        }
        echo json_encode($data);
    }
    else{
       array_push($data,array("error"=>"No university found "));
    }
}
else{
    return "No records found";
}