<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/24/2017
 * Time: 12:59 PM
 */
 const DBNAME ="ugc";
 const USERNAME="root";
 const PASSWORD="";
 const SERVERNAME="localhost";

// Create connection
$con = new mysqli(SERVERNAME, USERNAME,PASSWORD,DBNAME);

// Check connection
if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
}

if(!isset($_SESSION)){
      session_start();
}


$instalmentFields =  array(
      'amt' => array('value' => '',
                              'msg' => 'Instalment Ammount'
                        ),
      'cid' => array(
                              'value' => '',
                              'msg' => 'College ID'
                  )
      );

$studentFields = array(
      "cid" => array(
                  'value' => '',
                  'msg' => 'College'
            ),
      "year" => array(
                  'value' => '',
                  'msg' => 'Year'
            ),
      "total_stud" => array(
                  'value' => '',
                  'msg' => 'Total Student'
            ),
      "min_stud" => array(
                  'value' => '',
                  'msg' => 'Total Minority Student'
            ),
      "min_prop" => array(
                  'value' => '',
                  'msg' => 'Minority Proportion'
            )

);

$projectFields = array(
      "name" => array(
                  'value' => '',
                  'msg' => 'Project Name'
            ),
      "cid" => array(
                'value' => '',
               'msg' => 'College'
            ),
      "year" => array(
                  'value' => '',
                  'msg' => 'Year'
            ),
      "deptid" => array(
                  'value' => '',
                  'msg' => 'Department'
            ),
      "ramt" => array(
                  'value' => '',
                  'msg' => 'Required Ammount'
            ),
      "edu_level" => array(
                  'value' => '',
                  'msg' => 'Education Level'
            ),
      "type" => array(
                  'value' => '',
                  'msg' => 'Project type'
            ),
      "leader_name" => array(
                  'value' => '',
                  'msg' => 'Project Leader Name'
            ),
      "leader_phone" => array(
                  'value' => '',
                  'msg' => 'Project Leader Contact'
            ),
      "leader_mail" => array(
                  'value' => '',
                  'msg' => 'Project Leader Mail'
            )
 );


$collegeFields = array(
            "name" => array('value' => '',
                        'msg' => 'College Name'),
            "address" => array('value' => '',
                        'msg' => 'College Address'),
            "yoe" => array('value' => '',
                        'msg' => 'Year of Establishment'),
            "pincode" => array('value' => '',
                        'msg' => 'Pincode'),
            "affiliation" => array('value' => '',
                        'msg' => 'Nature of Affiliation'),
            "edu_level" => array('value' => '',
                        'msg' => 'Teaching upto'),
            "gov" => array('value' => '',
                        'msg' => 'College under'),
            "aided" => array('value' => '',
                        'msg' => 'College Organisation'),
            "12b" => array('value' => '',
                        'msg' => 'College 12b'),
            "12f" => array('value' => '',
                        'msg' => 'College 12f'),
            "naac" => array('value' => '',
                        'msg' => 'NAAC'),
            "contact" => array('value' => '',
                        'msg' => 'Contact'),
            "state" => array('value' => '',
                        'msg' => 'State'),
            "uni_id" => array('value' => '',
                        'msg' => 'University'),
            "colg_code" => array('value' => '',
                        'msg' => 'College code'),
            "password" => array('value' => '',
                        'msg' => 'Password')
          );