<?php
/**
 * Created by PhpStorm.
 * User: Franky Naidu
 * Date: 3/25/2017
 * Time: 2:30 PM
 */
//if we clicked on Upload button

if($_POST['Upload'] == 'Upload')

{

    //make the allowed extensions

    $goodExtensions = '.pdf';

    $error='';

    $uploaddir = 'UGC/'.$_SESSION["zone"].'/'.$_SESSION["university"];

    //set the current directory where you wanna upload the doc/docx files

    // ====================================================================================================

    // Year


    // ====================================================================================================

    // Division

    // ====================================================================================================

    // Category

    switch($_POST['zone'])
    {
        case 'W'   :  $uploaddir = './Files/English/';
            break;
        case 'Hindi'     :  $uploaddir = './Files/Hindi/';
            break;
        case 'Marathi'   :  $uploaddir = './Files/Marathi/';
            break;
        case 'Technical' :  $uploaddir = './Files/Technical/';
            break;
    }

    // ====================================================================================================

    $name = $_FILES['filename']['name'];//get the name of the file that will be uploaded

    $min_filesize=10;//set up a minimum file size

    $stem=substr($name,0,strpos($name,'.'));

    //take the file extension

    $extension = substr($name, strpos($name,'.'), strlen($name)-1);

    //verify if the file extension is valid or not

    if(!in_array($extension,$goodExtensions))

        $error.='Extension not allowed<br> Please Contact us at crce.edboard2017@gmail.com<br> ';

    echo "<span> </span>"; //verify if the file size of the file being uploaded is greater then 1

    if(filesize($_FILES['filename']['tmp_name']) < $min_filesize)

        $error.='File size too small<br>'."\n";

    $uploadfile = $uploaddir . $stem.$extension;

    $filename=$stem.$extension;

    if ($error=='')

    {

//upload the file to

        if (move_uploaded_file($_FILES['filename']['tmp_name'], $uploadfile)) {

            echo 'File Uploaded. Thank You. <br>For any queries contact us at : crce.edboard2017@gmail.com<br>';

        }

    }

    else echo $error;

}

?>