<?php
  require_once 'head.php';
  require_once 'sidebar.php';
?>
<div class="content-wrapper">
    <section class="content">
        <div class="container">

            <div class="row">
                <div class="span12">
                    <div class="hero-unit center">
                        <h1>College Not Found</h1>
                        <br />
                        <p>The college  you requested could not be found, The College might not be approved by the University.<br>  Use your browsers <b>Back</b> button to navigate to the page you have prevously come from</p>
                        <p><b>Or you could just press this neat little button:</b></p>
                        <?php
                            $ref = "";
                        if($_SESSION["type"] == "ugc")
                            $ref="ugcdashboard.php";
                            if($_SESSION["type"] == "college")
                                $ref="collegeDashboard.php?colg_id=".$_SESSION["cid"];
                            if($_SESSION["type"] == "university")
                                $ref="universityDashboard.php";
                        ?>
                        <a href="<?php echo $ref?>" class="btn btn-large btn-info"><i class="icon-home icon-white"></i> Take Me Home</a>
                    </div>
                    <br/>
                    <!-- By ConnerT HTML & CSS Enthusiast -->
                </div>
            </div>
        </div>
    </section>

</div>
<style>
    .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}

</style>