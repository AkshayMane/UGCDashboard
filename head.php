<?php
    if(!isset($_SESSION)){
        session_start();
    }
    $link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
    if(!isset($_SESSION['type']) && !stripos($link,"index.php") && !stripos($link,"login.php") && !stripos($link,"collegeForm.php")){
        header("Location: index.php");
        exit();
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UGCWebApp</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/adminLTE/bootstrap/css/bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!-- Full Calendar-->
    <link href='assets/AdminLTE/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
  <!-- Ionicons -->
  <link rel="stylesheet" href="assets/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/adminLTE/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="assets/adminLTE/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="assets/adminLTE/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="assets/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/adminLTE/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" type="text/css" href="assets/adminLTE/plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/adminLTE/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css">
    <link rel="stylesheet" href="assets/adminLTE/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="assets/adminLTE/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="assets/adminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- Morris charts -->
  <link rel="stylesheet" href="assets/adminLTE/plugins/morris/morris.css">
  <link href="css/card.css" rel="stylesheet" >

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- jQuery 2.2.3 -->
<script src="assets/adminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/adminLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="assets/adminLTE/plugins/fastclick/fastclick.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="assets/adminLTE/dist/js/demo.js"></script>

<script type="text/javascript" src="assets/adminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/adminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/adminLTE/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="assets/adminLTE/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="assets/adminLTE/plugins/chartjs/Chart.js"></script>
  <script src="assets/adminLTE/plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.6 -->
<!-- Morris.js charts -->
<script src="assets/raphael/raphael.min.js"></script>
<script src="assets/adminLTE/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="assets/adminLTE/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="assets/adminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="assets/adminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="assets/adminLTE/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="assets/moment/min/moment.min.js"></script>
<script src="assets/adminLTE/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="assets/adminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="assets/adminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!--Full Calendar  -->
<script src="assets/AdminLTE/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Slimscroll -->
<script src="assets/adminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/adminLTE/plugins/fastclick/fastclick.js"></script>
<script src="assets/adminLTE/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/tooltip.js"></script>
<!-- AdminLTE App -->
<script src="assets/adminLTE/dist/js/app.min.js"></script>
  <script  href="assets/adminLTE/plugins/ckeditor/ckeditor.js"></script>>
<script src="js/variables.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- <script src="js/dropdown.js"></script> -->
</head>