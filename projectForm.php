<?php
  require_once 'head.php';
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div id="header"></div>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        PROJECT FORM
      </h1>
      <div class="alert alert-dismissible" id="alertBox" style="display: none" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> <span id="alertMsg"></span></h4>
      </div>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active"><bold>General Elements</bold></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
            <form role="form" id="colg_form" enctype="multipart/form-data" action='<?php
            if(!isset($_GET['project_id'])){  echo "api/insertProject.php";  }else{ echo "api/updateProject.php";  } ?>' method="POST">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">REGISTER</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Project Name</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Enter your project name">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">College Name</label>
                  <select class="form-control" name="cid" id="cid">
                    <option selected disabled hidden>Select College</option>
                  </select>
                </div>
                
              
                <div class="form-group">
                  <label for="proposal">Upload your Proposal</label>
                  <input type="file" id="proposal" name="proposal">
                </div>

                  <div class="form-group">
                    <label>Year</label>
                    <input type="text" class="form-control" name="year" id="year" placeholder="Enter project year">
                  </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Project Type</label>
                    <select id="type" name="type" class="form-control">
        <option selected disabled hidden>Select Project Type</option>
                      <option value="H">Hardware</option>
                      <option value="S">Software</option>
                      <option value="R">Research</option>
                      <option value="G">Greenhouse</option>
                    </select>
                  </div>
                </div>
 <!-- select -->
                <div class="col-md-6">
                <div class="form-group">
                  <label>Department</label>
                  <select name="deptid" id="did" class="form-control">
		    <option selected disabled hidden>Select Department</option>
                  </select>
                </div>
                </div>
	              <div class="form-group">
                  <label for="exampleInputEmail1">Amount</label>
                  <input type="number" name="ramt" class="form-control" id="ramt" placeholder="Enter the required amount">
                </div>
	               <label for="teachingUpto">Project Education level</label>
                    <select name="edu_level" class="form-control" id="edu_level">
                      <option selected value="" disabled hidden>Select Option</option>
                      <option value="H">High Secondary</option>
                      <option value="B">UG</option>
                      <option value="M">PG</option>
                      <option value="P">PHD</option>
                    </select>
                  </div>
                  </div>
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Project Leader Details</h3>
                </div>  
                <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="leader_name" class="form-control" id="leader_name" placeholder="Enter Leader Name">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Contact</label>
                  <input type="text" name="leader_phone" class="form-control" id="leader_phone" placeholder="Enter Leader Contact">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email Id</label>
                  <input type="text" name="leader_mail" class="form-control" id="leader_mail" placeholder="Enter Leader Email Id">
                </div>

                    <div class="progress" style="display: none">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"><span class="sr-only"></span>
                        </div>
                    </div>

                </div>              <!-- /.box-body -->
                </div>
                <?php
                  if(isset($_GET['project_id']) and $_GET['project_id'] != ''){
                    echo '<input style="display:none" name="project_id" value="'.$_GET['project_id'].'" />';
                  }
                ?>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          <!-- /.box -->
          </div>
          </div>
          </section>
          </div>
          <div id="footer"></div>
          <?php
          if(!isset($_GET['project_id'])){
          ?>
          <script type="text/javascript" src="js/insertProject.js"></script>
          <?php
          }else{
          ?>
          <script type="text/javascript" src="js/updateProject.js"></script>
          <?php
          }
          ?>
          <script type="text/javascript" src="js/dropdown.js"></script>
          <script src="assets/jquery-form/dist/jquery.form.min.js" ></script>
          <script type="text/javascript">
    $(function () {
        /*Load The header*/
        $('#header').load("header.php");
        $('#footer').load("footer.php")

    });
</script>
</body>
</html>
